## Plotting scripts for monoV

### How to check out this package

1. Open a clean terminal
2. Make sure you have a [kerberos token](http://linux.web.cern.ch/linux/docs/kerberos-access.shtml) for `CERN.ch`
3. Enter `git clone https://:@gitlab.cern.ch:8443/pgadow/Plotting_monoVH.git`
4. Enter `cd Plotting_monoVH`

### How to make plots

1. Open [macros/runMonoVHPlots.cxx](https://gitlab.cern.ch/pgadow/Plotting_monoVH/blob/master/macros/runMonoVHPlots.cxx)
    and add the correct hard-coded paths to your input histogram ROOT files and set the output path
    
2. Setup you local ROOT distribution (e.g. `setupATLAS` and `lsetup root` on lxplus)
 
3. Start plotting steering script

```
root -b -q -l macros/runMonoVHPlots.cxx\(0\) # 0 lepton plots
root -b -q -l macros/runMonoVHPlots.cxx\(1\) # 1 lepton plots
root -b -q -l macros/runMonoVHPlots.cxx\(2\) # 2 lepton plots
root -b -q -l macros/runMonoVHPlots.cxx\(3\) # 0 lepton QCD plots
```

You can modify the plotting scripts to change the variables and regions for which plots are created.

### How to make systematic variation up/down plots

1. Enter `cd systematics`
2. Modify [systematics/StudySystematicsFull.py](https://gitlab.cern.ch/pgadow/Plotting_monoVH/blob/master/systematics/StudySystematicsFull.py) to make sure the systematic names are correctly entered
3. Modify [systematics/run.sh](https://gitlab.cern.ch/pgadow/Plotting_monoVH/blob/master/systematics/run.sh) to make sure the correct input file and analysis regions are set up.
4. Enter `bash run.sh`