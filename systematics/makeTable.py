import csv
from argparse import ArgumentParser


def getArgs():
    """Get arguments from command line."""
    parser = ArgumentParser()
    parser.add_argument('inputFile')
    return parser.parse_args()


def main():
    """read csv file and sum up merged and resolved."""
    args = getArgs()
    np_bkg_nom_merged = {}
    np_bkg_up_merged = {}
    np_bkg_do_merged = {}

    np_sig1_nom_merged = {}
    np_sig1_up_merged = {}
    np_sig1_do_merged = {}

    np_sig2_nom_merged = {}
    np_sig2_up_merged = {}
    np_sig2_do_merged = {}

    np_bkg_nom_resolved = {}
    np_bkg_up_resolved = {}
    np_bkg_do_resolved = {}

    np_sig1_nom_resolved = {}
    np_sig1_up_resolved = {}
    np_sig1_do_resolved = {}

    np_sig2_nom_resolved = {}
    np_sig2_up_resolved = {}
    np_sig2_do_resolved = {}

    with open(args.inputFile, 'r') as csvfile:
        reader = csv.reader(csvfile)
        for row in reader:
            region = row[0]
            name = row[1]
            bkg_nom = float(row[2])
            bkg_up = float(row[3])
            bkg_do = float(row[4])
            sig1_nom = float(row[5])
            sig1_up = float(row[6])
            sig1_do = float(row[7])
            sig2_nom = float(row[8])
            sig2_up = float(row[9])
            sig2_do = float(row[10])

            # is merged
            if '1pfat' in region:

                if name in np_bkg_nom_merged:
                    np_bkg_nom_merged[name] += bkg_nom
                else:
                    np_bkg_nom_merged[name] = bkg_nom

                if name in np_bkg_up_merged:
                    np_bkg_up_merged[name] += bkg_up
                else:
                    np_bkg_up_merged[name] = bkg_up

                if name in np_bkg_do_merged:
                    np_bkg_do_merged[name] += bkg_do
                else:
                    np_bkg_do_merged[name] = bkg_do

                if name in np_sig1_nom_merged:
                    np_sig1_nom_merged[name] += sig1_nom
                else:
                    np_sig1_nom_merged[name] = sig1_nom

                if name in np_sig1_up_merged:
                    np_sig1_up_merged[name] += sig1_up
                else:
                    np_sig1_up_merged[name] = sig1_up

                if name in np_sig1_do_merged:
                    np_sig1_do_merged[name] += sig1_do
                else:
                    np_sig1_do_merged[name] = sig1_do

                if name in np_sig2_nom_merged:
                    np_sig2_nom_merged[name] += sig2_nom
                else:
                    np_sig2_nom_merged[name] = sig2_nom

                if name in np_sig2_up_merged:
                    np_sig2_up_merged[name] += sig2_up
                else:
                    np_sig2_up_merged[name] = sig2_up

                if name in np_sig2_do_merged:
                    np_sig2_do_merged[name] += sig2_do
                else:
                    np_sig2_do_merged[name] = sig2_do

            # is resolved
            if '0pfat' in region:
                if name in np_bkg_nom_resolved:
                    np_bkg_nom_resolved[name] += bkg_nom
                else:
                    np_bkg_nom_resolved[name] = bkg_nom

                if name in np_bkg_up_resolved:
                    np_bkg_up_resolved[name] += bkg_up
                else:
                    np_bkg_up_resolved[name] = bkg_up

                if name in np_bkg_do_resolved:
                    np_bkg_do_resolved[name] += bkg_do
                else:
                    np_bkg_do_resolved[name] = bkg_do

                if name in np_sig1_nom_resolved:
                    np_sig1_nom_resolved[name] += sig1_nom
                else:
                    np_sig1_nom_resolved[name] = sig1_nom

                if name in np_sig1_up_resolved:
                    np_sig1_up_resolved[name] += sig1_up
                else:
                    np_sig1_up_resolved[name] = sig1_up

                if name in np_sig1_do_resolved:
                    np_sig1_do_resolved[name] += sig1_do
                else:
                    np_sig1_do_resolved[name] = sig1_do

                if name in np_sig2_nom_resolved:
                    np_sig2_nom_resolved[name] += sig2_nom
                else:
                    np_sig2_nom_resolved[name] = sig2_nom

                if name in np_sig2_up_resolved:
                    np_sig2_up_resolved[name] += sig2_up
                else:
                    np_sig2_up_resolved[name] = sig2_up

                if name in np_sig2_do_resolved:
                    np_sig2_do_resolved[name] += sig2_do
                else:
                    np_sig2_do_resolved[name] = sig2_do

    line1 = "Systematic Source & Background Variation & %s & %s \\\\ \\midrule \n" % ("dmVVhadDM1MM600", "dmVVhadDM1MM600")
    line2 = "& (up,down)  & (up,down) & (up,down)   \\\\ \\midrule \n"
    print(line1)
    print(line2)

    for name in np_bkg_nom_merged.keys():
        if "MassFail" in name:
            continue
        try:
            up_bkg = float(np_bkg_up_merged[name]) / float(np_bkg_nom_merged[name])
            do_bkg = float(np_bkg_do_merged[name]) / float(np_bkg_nom_merged[name])
        except ZeroDivisionError:
            up_bkg = 0.
            do_bkg = 0.
            if "SysMJ_Shape" not in name:
                print("oops, i didn't like to divide by 0 for ", name)

        try:
            up_sig1 = float(np_sig1_up_merged[name]) / float(np_sig1_nom_merged[name])
            do_sig1 = float(np_sig1_do_merged[name]) / float(np_sig1_nom_merged[name])
        except ZeroDivisionError:
            up_sig1 = 0.
            do_sig1 = 0.
            if "SysMJ_Shape" not in name:
                print("oops, i didn't like to divide by 0 for ", name)

        try:
            up_sig2 = float(np_sig2_up_merged[name]) / float(np_sig2_nom_merged[name])
            do_sig2 = float(np_sig2_do_merged[name]) / float(np_sig2_nom_merged[name])
        except ZeroDivisionError:
            up_sig2 = 0.
            do_sig2 = 0.
            if "SysMJ_Shape" not in name:
                print("oops, i didn't like to divide by 0 for ", name)

        line = "{name} & ({up_bkg:.4f}, {do_bkg:.4f}) & ({up_sig1:.4f}, {do_sig1:.4f}) & ({up_sig2:.4f}, {do_sig2:.4f}) \\\\".format(name=name, up_bkg=up_bkg, do_bkg=do_bkg, up_sig1=up_sig1, do_sig1=do_sig1, up_sig2=up_sig2, do_sig2=do_sig2)
        print(line)

    line1 = "Systematic Source & Background Variation & %s & %s \\\\ \\midrule \n" % ("dmVVhadDM1MM600", "dmVVhadDM1MM600")
    line2 = "& (up,down)  & (up,down) & (up,down)   \\\\ \\midrule \n"
    print(line1)
    print(line2)

    for name in np_bkg_nom_resolved.keys():
        if "MassFail" in name:
            continue
        try:
            up_bkg = float(np_bkg_up_resolved[name]) / float(np_bkg_nom_resolved[name])
            do_bkg = float(np_bkg_do_resolved[name]) / float(np_bkg_nom_resolved[name])
        except ZeroDivisionError:
            up_bkg = 0.
            do_bkg = 0.
            if "SysMJ_Shape" not in name:
                print("oops, i didn't like to divide by 0 for ", name)

        try:
            up_sig1 = float(np_sig1_up_resolved[name]) / float(np_sig1_nom_resolved[name])
            do_sig1 = float(np_sig1_do_resolved[name]) / float(np_sig1_nom_resolved[name])
        except ZeroDivisionError:
            up_sig1 = 0.
            do_sig1 = 0.
            if "SysMJ_Shape" not in name:
                print("oops, i didn't like to divide by 0 for ", name)

        try:
            up_sig2 = float(np_sig2_up_resolved[name]) / float(np_sig2_nom_resolved[name])
            do_sig2 = float(np_sig2_do_resolved[name]) / float(np_sig2_nom_resolved[name])
        except ZeroDivisionError:
            up_sig2 = 0.
            do_sig2 = 0.
            if "SysMJ_Shape" not in name:
                print("oops, i didn't like to divide by 0 for ", name)

        line = "{name} & ({up_bkg:.4f}, {do_bkg:.4f}) & ({up_sig1:.4f}, {do_sig1:.4f}) & ({up_sig2:.4f}, {do_sig2:.4f}) \\\\".format(name=name, up_bkg=up_bkg, do_bkg=do_bkg, up_sig1=up_sig1, do_sig1=do_sig1, up_sig2=up_sig2, do_sig2=do_sig2)
        print(line)

#     file.write(line1)
#     file.write(line2)                


if __name__ == '__main__':
    main()
