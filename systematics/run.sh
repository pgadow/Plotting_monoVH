#! /bin/sh
# @Author: Paul Philipp Gadow
# @Date:   2017-07-04 16:06:43
# @Last Modified by:   Paul Philipp Gadow
# @Last Modified time: 2017-07-04 18:58:12

# ------------
# set up
# ------------
export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh
lsetup root xrootd


FITINPUTS=/eos/atlas/atlascerngroupdisk/phys-exotics/jdm/monoV/Run2/MonoVfittingInput2017-10-26
# ------------
# 0 lepton
# ------------

# get input file from EOS
# xrdcp root://eosatlas.cern.ch/${FITINPUTS}/monoV_13TeV_0lep_mj.root MonoVFitInputTightWithMJ_13TeV_0lep.root

# enter here the hadded root file containing fit inputs
INPUTFILE=./MonoVFitInputTightWithMJ_13TeV_0lep.root
PRUNINGOPTION=False

# systematics in W/Z mass window
python StudySystematicsFull.py -i $INPUTFILE -s dmVVhadDM1MM600 dmVVhadDM1MM200 -l 0lep -r 0tag1pfat0pjet_0ptv_0lep_SR_SubstrPassMassPass --pruning ${PRUNINGOPTION}
python StudySystematicsFull.py -i $INPUTFILE -s dmVVhadDM1MM600 dmVVhadDM1MM200 -l 0lep -r 1tag1pfat0pjet_0ptv_0lep_SR_SubstrPassMassPass --pruning ${PRUNINGOPTION}
python StudySystematicsFull.py -i $INPUTFILE -s dmVVhadDM1MM600 dmVVhadDM1MM200 -l 0lep -r 2tag1pfat0pjet_0ptv_0lep_SR_MassPass --pruning ${PRUNINGOPTION}

# systematics in W/Z mass window low purity signal regions 
python StudySystematicsFull.py -i $INPUTFILE -s dmVVhadDM1MM600 dmVVhadDM1MM200 -l 0lep -r 0tag1pfat0pjet_0ptv_0lep_SR_SubstrFailMassPass --pruning ${PRUNINGOPTION}
python StudySystematicsFull.py -i $INPUTFILE -s dmVVhadDM1MM600 dmVVhadDM1MM200 -l 0lep -r 1tag1pfat0pjet_0ptv_0lep_SR_SubstrFailMassPass --pruning ${PRUNINGOPTION}

# systematics in W/Z mass window
python StudySystematicsFull.py -i $INPUTFILE -s dmVVhadDM1MM600 dmVVhadDM1MM200 -l 0lep -r 0tag0pfat0pjet_0ptv_0lep_SR_MassPass --pruning ${PRUNINGOPTION}
python StudySystematicsFull.py -i $INPUTFILE -s dmVVhadDM1MM600 dmVVhadDM1MM200 -l 0lep -r 1tag0pfat0pjet_0ptv_0lep_SR_MassPass --pruning ${PRUNINGOPTION}
python StudySystematicsFull.py -i $INPUTFILE -s dmVVhadDM1MM600 dmVVhadDM1MM200 -l 0lep -r 2tag0pfat0pjet_0ptv_0lep_SR_MassPass --pruning ${PRUNINGOPTION}



# # systematics in W/Z mass side-bands
# python StudySystematicsFull.py -i $INPUTFILE -s dmVWhadDM1MM300 dmVZhadDM1MM300 -l 0lep -r 0tag1pfat0pjet_0ptv_0lep_SR_SubstrPassMassFailHigh --pruning ${PRUNINGOPTION}
# python StudySystematicsFull.py -i $INPUTFILE -s dmVWhadDM1MM300 dmVZhadDM1MM300 -l 0lep -r 1tag1pfat0pjet_0ptv_0lep_SR_SubstrPassMassFailHigh --pruning ${PRUNINGOPTION}
# python StudySystematicsFull.py -i $INPUTFILE -s dmVWhadDM1MM300 dmVZhadDM1MM300 -l 0lep -r 2tag1pfat0pjet_0ptv_0lep_SR_MassFailHigh --pruning ${PRUNINGOPTION}

# # systematics in W/Z mass window low purity side-bands
# python StudySystematicsFull.py -i $INPUTFILE -s dmVVhadDM1MM600 dmVVhadDM1MM200 -l 0lep -r 0tag1pfat0pjet_0ptv_0lep_SR_SubstrFailMassFailHigh --pruning ${PRUNINGOPTION}
# python StudySystematicsFull.py -i $INPUTFILE -s dmVVhadDM1MM600 dmVVhadDM1MM200 -l 0lep -r 1tag1pfat0pjet_0ptv_0lep_SR_SubstrFailMassFailHigh --pruning ${PRUNINGOPTION}

# # systematics in W/Z mass side-bands
# python StudySystematicsFull.py -i $INPUTFILE -s dmVWhadDM1MM300 dmVZhadDM1MM300 -l 0lep -r 0tag0pfat0pjet_0ptv_0lep_SR_MassFailHigh --pruning ${PRUNINGOPTION}
# python StudySystematicsFull.py -i $INPUTFILE -s dmVWhadDM1MM300 dmVZhadDM1MM300 -l 0lep -r 1tag0pfat0pjet_0ptv_0lep_SR_MassFailHigh --pruning ${PRUNINGOPTION}
# python StudySystematicsFull.py -i $INPUTFILE -s dmVWhadDM1MM300 dmVZhadDM1MM300 -l 0lep -r 2tag0pfat0pjet_0ptv_0lep_SR_MassFailHigh --pruning ${PRUNINGOPTION}
# # ------------
# # 1 lepton
# # ------------

# # get input file from EOS
# xrdcp root://eosatlas.cern.ch/${FITINPUTS}/monoV_13TeV_1lep.root MonoVFitInput_13TeV_1lep.root

# # enter here the hadded root file containing fit inputs
# INPUTFILE=./MonoVFitInput_13TeV_1lep.root

# # systematics in W/Z mass window
# python StudySystematicsFull.py -i $INPUTFILE -l 1lep -r 0tag1pfat0pjet_0ptv_1lep_CR_MassPass --pruning ${PRUNINGOPTION}
# python StudySystematicsFull.py -i $INPUTFILE -l 1lep -r 1tag1pfat0pjet_0ptv_1lep_CR_MassPass --pruning ${PRUNINGOPTION}
# python StudySystematicsFull.py -i $INPUTFILE -l 1lep -r 2tag1pfat0pjet_0ptv_1lep_CR_MassPass --pruning ${PRUNINGOPTION}
# python StudySystematicsFull.py -i $INPUTFILE -l 1lep -r 0tag0pfat0pjet_0ptv_1lep_CR_MassPass --pruning ${PRUNINGOPTION}
# python StudySystematicsFull.py -i $INPUTFILE -l 1lep -r 1tag0pfat0pjet_0ptv_1lep_CR_MassPass --pruning ${PRUNINGOPTION}
# python StudySystematicsFull.py -i $INPUTFILE -l 1lep -r 2tag0pfat0pjet_0ptv_1lep_CR_MassPass --pruning ${PRUNINGOPTION}

# # systematics in W/Z mass side-bands
# python StudySystematicsFull.py -i $INPUTFILE -l 1lep -r 0tag1pfat0pjet_0ptv_1lep_CR_MassFailHigh --pruning ${PRUNINGOPTION}
# python StudySystematicsFull.py -i $INPUTFILE -l 1lep -r 1tag1pfat0pjet_0ptv_1lep_CR_MassFailHigh --pruning ${PRUNINGOPTION}
# python StudySystematicsFull.py -i $INPUTFILE -l 1lep -r 2tag1pfat0pjet_0ptv_1lep_CR_MassFailHigh --pruning ${PRUNINGOPTION}
# python StudySystematicsFull.py -i $INPUTFILE -l 1lep -r 0tag0pfat0pjet_0ptv_1lep_CR_MassFailHigh --pruning ${PRUNINGOPTION}
# python StudySystematicsFull.py -i $INPUTFILE -l 1lep -r 1tag0pfat0pjet_0ptv_1lep_CR_MassFailHigh --pruning ${PRUNINGOPTION}
# python StudySystematicsFull.py -i $INPUTFILE -l 1lep -r 2tag0pfat0pjet_0ptv_1lep_CR_MassFailHigh --pruning ${PRUNINGOPTION}

# # ------------
# # 2 lepton
# # ------------

# # get input file from EOS
# xrdcp root://eosatlas.cern.ch/${FITINPUTS}/monoV_13TeV_2lep.root MonoVFitInput_13TeV_2lep.root
# # enter here the hadded root file containing fit inputs
# INPUTFILE=./MonoVFitInput_13TeV_2lep.root

# # systematics in W/Z mass window
# python StudySystematicsFull.py -i $INPUTFILE -l 2lep -r 0tag1pfat0pjet_0ptv_2lep_CR_MassPass --pruning ${PRUNINGOPTION}
# python StudySystematicsFull.py -i $INPUTFILE -l 2lep -r 1tag1pfat0pjet_0ptv_2lep_CR_MassPass --pruning ${PRUNINGOPTION}
# python StudySystematicsFull.py -i $INPUTFILE -l 2lep -r 2tag1pfat0pjet_0ptv_2lep_CR_MassPass --pruning ${PRUNINGOPTION}
# python StudySystematicsFull.py -i $INPUTFILE -l 2lep -r 0tag0pfat0pjet_0ptv_2lep_CR_MassPass --pruning ${PRUNINGOPTION}
# python StudySystematicsFull.py -i $INPUTFILE -l 2lep -r 1tag0pfat0pjet_0ptv_2lep_CR_MassPass --pruning ${PRUNINGOPTION}
# python StudySystematicsFull.py -i $INPUTFILE -l 2lep -r 2tag0pfat0pjet_0ptv_2lep_CR_MassPass --pruning ${PRUNINGOPTION}

# # systematics in W/Z mass side-bands
# python StudySystematicsFull.py -i $INPUTFILE -l 2lep -r 0tag1pfat0pjet_0ptv_2lep_CR_MassFailHigh --pruning ${PRUNINGOPTION}
# python StudySystematicsFull.py -i $INPUTFILE -l 2lep -r 1tag1pfat0pjet_0ptv_2lep_CR_MassFailHigh --pruning ${PRUNINGOPTION}
# python StudySystematicsFull.py -i $INPUTFILE -l 2lep -r 2tag1pfat0pjet_0ptv_2lep_CR_MassFailHigh --pruning ${PRUNINGOPTION}
# python StudySystematicsFull.py -i $INPUTFILE -l 2lep -r 0tag0pfat0pjet_0ptv_2lep_CR_MassFailHigh --pruning ${PRUNINGOPTION}
# python StudySystematicsFull.py -i $INPUTFILE -l 2lep -r 1tag0pfat0pjet_0ptv_2lep_CR_MassFailHigh --pruning ${PRUNINGOPTION}
# python StudySystematicsFull.py -i $INPUTFILE -l 2lep -r 2tag0pfat0pjet_0ptv_2lep_CR_MassFailHigh --pruning ${PRUNINGOPTION}
