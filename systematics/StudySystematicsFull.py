######################
# Author : Sam Meehan <samuel.meehan@cern.ch>
#          Philipp Gadow <Paul.Philipp.Gadow@cern.ch>
#
# This is a script that is used to produce the content for the
# section on systematic variations in the mono-V(had) internal note.
#
# Running:
# $ python StudySystematicsFull.py -i sys0lep_v28_009.root -s dmVWhadDM1MM300 dmVZhadDM1MM300 -l 0lep
######################

import os
import math
import argparse
from ROOT import *
from AtlasStyle import *
from LocalFunctions import *

# ROOT settings
gROOT.SetBatch(1)
SetAtlasStyle()
gStyle.SetPalette(1)
TH1.SetDefaultSumw2()


def getArgs():
  parser = argparse.ArgumentParser(description="Script to study experimental systematic uncertainties")
  parser.add_argument('-i', '--input', dest='inputFile', help='Input file', required=True)
  parser.add_argument('-o', '--output', dest='outputDir', help='Directory for your output', required=False)
  parser.add_argument('-r', '--region', dest='region', help='Region where to study systematics', required=False, default="0tag1pfat0pjet_0ptv_0lep_SR_SubstrPassMassPass")
  parser.add_argument('-l', '--lepton', dest='lepton', help='Define number leptons for signal or control region', required=False, default="0lep")
  parser.add_argument('-s', '--signals', dest='signals', help='Define the signals you want to study', required=False, nargs='+', default=["dmVWhadDM1MM300", "dmVZhadDM1MM300"])
  parser.add_argument('-p', '--pruning', dest='pruning', help='pruning of systematics', required=False, default='True')
  parser.add_argument('-d', '--debug', dest='debugMode', help='Run in debug mode', required=False, default='False')

  return parser.parse_args()


def main():
  # set up options
  options       = getArgs()
  f             = TFile(str(options.inputFile))
  region_sr     = str(options.region)
  debugmode     = str(options.debugMode)
  pruning      = str(options.pruning)
  nLeptons      = str(options.lepton)

  # histogram settings
  # 0 lepton SR
  variable = "MET"
  xlabel   = "E_{T}^{miss} [GeV]"
  rebin    = 50
  xmin     = 100
  xmax     = 1500
  ymax     = 0.7

  # 1 lepton CR
  if options.lepton == '1lep':
    # choose METMod:
    variable = "MU_METMod"
    xlabel   = "E_{T}^{miss} (mod.) [GeV]"


  # 2 lepton CR
  elif options.lepton == '2lep':
    variable = "METMod"
    xlabel   = "E_{T}^{miss} (mod.) [GeV]"


  # sample names
  name_zbb    = "Zbb"
  name_zbc    = "Zbc"
  name_zbl    = "Zbl"
  name_zcc    = "Zcc"
  name_zcl    = "Zcl"
  name_zl     = "Zl"
  name_wbb   = "Wbb"
  name_wbc   = "Wbc"
  name_wbl   = "Wbl"
  name_wcc   = "Wcc"
  name_wcl   = "Wcl"
  name_wl    = "Wl"
  name_tt     = "ttbar"
  name_stops  = "stops"
  name_stopt  = "stopt"
  name_stopwt = "stopWt"
  name_ww     = "WW"
  name_wz     = "WZ"
  name_zz     = "ZZ"

  # multijet background
  if options.lepton == '0lep':
    name_mj0tagmergedmasspass = "multijet0mergedMassPass"
    name_mj1tagmergedmasspass = "multijet1mergedMassPass"
    name_mj2tagmergedmasspass = "multijet2mergedMassPass"
    name_mj0tagmergedmassfail = "multijet0mergedMassFail"
    name_mj1tagmergedmassfail = "multijet1mergedMassFail"
    name_mj2tagmergedmassfail = "multijet2mergedMassFail"
    name_mj0tagresolvedmasspass = "multijet0resolvedMassPass"
    name_mj1tagresolvedmasspass = "multijet1resolvedMassPass"
    name_mj2tagresolvedmasspass = "multijet2resolvedMassPass"
    name_mj0tagresolvedmassfail = "multijet0resolvedMassFail"
    name_mj1tagresolvedmassfail = "multijet1resolvedMassFail"
    name_mj2tagresolvedmassfail = "multijet2resolvedMassFail"

  if len(options.signals) == 1:
    basename_sig1 = str(options.signals[0])
    basename_sig2 = str(options.signals[0])
  if len(options.signals) == 2:
    basename_sig1 = str(options.signals[0])
    basename_sig2 = str(options.signals[1])


  is_dmVVhad_sig1 = False
  if "dmVVhad" in basename_sig1:
    is_dmVVhad_sig1 = True
  is_dmVVhad_sig2 = False
  if "dmVVhad" in basename_sig2:
    is_dmVVhad_sig2 = True


  outputdir = ""
  print options.outputDir

  if options.outputDir == None:
    outputdir = "Plots_Systematics_Full_"+region_sr
  else:
    outputdir = str(options.outputDir)

  if nLeptons != None:
    outputdir = str(outputdir).replace("Full_","Full_"+nLeptons+"_")
  print outputdir

  os.system("mkdir "+outputdir)

  RegionsAndVariables=[]
  RegionsAndVariables.append([region_sr, variable])

  SystSizes={}

  csvName = outputdir+"/csv_"+basename_sig1+"_"+basename_sig2+"_"+nLeptons+"_"+region_sr+".csv"
  csvFile = open(csvName, 'w')

  for RegionAndVariable in RegionsAndVariables:

      region   = RegionAndVariable[0]
      variable = RegionAndVariable[1]

      TempSystSizes={}

      SystVariations=[]

    # Systematics on Pileup Reweighting
      SystVariations.append(["SysPRW_DATASF__1up", "SysPRW_DATASF__1down"])

    # Systematics on Trigger SF
      if options.lepton == '0lep' or options.lepton == '1lep':
        SystVariations.append(["SysMETTrigStat__1up", "SysMETTrigStat__1down"])
        SystVariations.append(["SysMETTrigSyst__1up", "SysMETTrigSyst__1down"])

    # Systematics on MET/MPT
      SystVariations.append(["SysMET_JetTrk_Scale__1up",    "SysMET_JetTrk_Scale__1down"])
      SystVariations.append(["SysMET_SoftTrk_ResoPara__1up"])
      SystVariations.append(["SysMET_SoftTrk_ResoPerp__1up"])
      SystVariations.append(["SysMET_SoftTrk_Scale__1up",    "SysMET_SoftTrk_Scale__1down"])

    # Systematics on electrons
      SystVariations.append(["SysEG_RESOLUTION_ALL__1up"                     , "SysEG_RESOLUTION_ALL__1down"])
      SystVariations.append(["SysEG_SCALE_ALL__1up"                          , "SysEG_SCALE_ALL__1down"])
      SystVariations.append(["SysEL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR__1up"     , "SysEL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR__1down"])
      SystVariations.append(["SysEL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR__1up"    , "SysEL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR__1down"])
      SystVariations.append(["SysEL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR__1up"   , "SysEL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR__1down"])
      SystVariations.append(["SysEL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR__1up", "SysEL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR__1down"])

    # Systematics on muons
      SystVariations.append(["SysMUON_SCALE__1up"                  , "SysMUON_SCALE__1down"])
      SystVariations.append(["SysMUON_ID__1up"                     , "SysMUON_ID__1down"])
      SystVariations.append(["SysMUON_MS__1up"                     , "SysMUON_MS__1down"])
      SystVariations.append(["SysMUON_EFF_STAT__1up"               , "SysMUON_EFF_STAT__1down"])
      SystVariations.append(["SysMUON_EFF_SYS__1up"                , "SysMUON_EFF_SYS__1down"])
      # SystVariations.append(["SysMUON_EFF_STAT_LOWPT__1up"         , "SysMUON_EFF_STAT_LOWPT__1down"])
      # SystVariations.append(["SysMUON_EFF_SYS_LOWPT__1up"          , "SysMUON_EFF_SYS_LOWPT__1down"])
      SystVariations.append(["SysMUON_ISO_SYS__1up"                , "SysMUON_ISO_SYS__1down"])
      SystVariations.append(["SysMUON_ISO_STAT__1up"               , "SysMUON_ISO_STAT__1down"])
      SystVariations.append(["SysMUON_TTVA_STAT__1up"              , "SysMUON_TTVA_STAT__1down"])
      SystVariations.append(["SysMUON_TTVA_SYS__1up"               , "SysMUON_TTVA_SYS__1down"])
      # muon trigger is only used in 2 lepton region
      if options.lepton == '2lep':
        SystVariations.append(["SysMUON_EFF_TrigStatUncertainty__1up", "SysMUON_EFF_TrigStatUncertainty__1down"])
        SystVariations.append(["SysMUON_EFF_TrigSysUncertainty__1up" , "SysMUON_EFF_TrigSysUncertainty__1down"])

    # Systematics on anti-kt 0.4 jets (strongly reduced NP scheme) - choose this or 21 NP scheme in the analysis
      SystVariations.append(["SysJET_JER_SINGLE_NP__1up"])
      SystVariations.append(["SysJET_JvtEfficiency__1up"                              ,"SysJET_JvtEfficiency__1down"])
      SystVariations.append(["SysJET_SR1_JET_GroupedNP_1__1up"                    ,"SysJET_SR1_JET_GroupedNP_1__1down"])
      SystVariations.append(["SysJET_SR1_JET_GroupedNP_2__1up"                    ,"SysJET_SR1_JET_GroupedNP_2__1down"])
      SystVariations.append(["SysJET_SR1_JET_GroupedNP_3__1up"                    ,"SysJET_SR1_JET_GroupedNP_3__1down"])
      SystVariations.append(["SysJET_SR1_JET_EtaIntercalibration_NonClosure__1up" ,"SysJET_SR1_JET_EtaIntercalibration_NonClosure__1down"])

    # Systematics on anti-kt 0.4 jets (21 NP scheme)  - choose this or strongly reduced NP scheme in the analysis
      # SystVariations.append(["SysJET_JER_SINGLE_NP__1up"])
      # SystVariations.append(["SysJET_JvtEfficiency__1up"                             ,"SysJET_JvtEfficiency__1down"])
      # SystVariations.append(["SysJET_21NP_JET_EffectiveNP_1__1up"                    ,"SysJET_21NP_JET_EffectiveNP_1__1down"])
      # SystVariations.append(["SysJET_21NP_JET_EffectiveNP_2__1up"                    ,"SysJET_21NP_JET_EffectiveNP_2__1down"])
      # SystVariations.append(["SysJET_21NP_JET_EffectiveNP_3__1up"                    ,"SysJET_21NP_JET_EffectiveNP_3__1down"])
      # SystVariations.append(["SysJET_21NP_JET_EffectiveNP_4__1up"                    ,"SysJET_21NP_JET_EffectiveNP_4__1down"])
      # SystVariations.append(["SysJET_21NP_JET_EffectiveNP_5__1up"                    ,"SysJET_21NP_JET_EffectiveNP_5__1down"])
      # SystVariations.append(["SysJET_21NP_JET_EffectiveNP_6__1up"                    ,"SysJET_21NP_JET_EffectiveNP_6__1down"])
      # SystVariations.append(["SysJET_21NP_JET_EffectiveNP_7__1up"                    ,"SysJET_21NP_JET_EffectiveNP_7__1down"])
      # SystVariations.append(["SysJET_21NP_JET_EffectiveNP_8restTerm__1up"            ,"SysJET_21NP_JET_EffectiveNP_8restTerm__1down"])
      # SystVariations.append(["SysJET_21NP_JET_EtaIntercalibration_Modelling__1up"    ,"SysJET_21NP_JET_EtaIntercalibration_Modelling__1down"])
      # SystVariations.append(["SysJET_21NP_JET_EtaIntercalibration_TotalStat__1up"    ,"SysJET_21NP_JET_EtaIntercalibration_TotalStat__1down"])
      # SystVariations.append(["SysJET_21NP_JET_EtaIntercalibration_NonClosure__1up"   ,"SysJET_21NP_JET_EtaIntercalibration_NonClosure__1down"])
      # SystVariations.append(["SysJET_21NP_JET_Pileup_OffsetMu__1up"                  ,"SysJET_21NP_JET_Pileup_OffsetMu__1down"])
      # SystVariations.append(["SysJET_21NP_JET_Pileup_OffsetNPV__1up"                 ,"SysJET_21NP_JET_Pileup_OffsetNPV__1down"])
      # SystVariations.append(["SysJET_21NP_JET_Pileup_PtTerm__1up"                    ,"SysJET_21NP_JET_Pileup_PtTerm__1down"])
      # SystVariations.append(["SysJET_21NP_JET_Pileup_RhoTopology__1up"               ,"SysJET_21NP_JET_Pileup_RhoTopology__1down"])
      # SystVariations.append(["SysJET_21NP_JET_Flavor_Composition__1up"               ,"SysJET_21NP_JET_Flavor_Composition__1down"])
      # SystVariations.append(["SysJET_21NP_JET_Flavor_Response__1up"                  ,"SysJET_21NP_JET_Flavor_Response__1down"])
      # SystVariations.append(["SysJET_21NP_JET_BJES_Response__1up"                    ,"SysJET_21NP_JET_BJES_Response__1down"])
      # SystVariations.append(["SysJET_21NP_JET_PunchThrough_MC15__1up"                ,"SysJET_21NP_JET_PunchThrough_MC15__1down"])
      # SystVariations.append(["SysJET_21NP_JET_SingleParticle_HighPt__1up"            ,"SysJET_21NP_JET_SingleParticle_HighPt__1down"])


    # Systematics on anti-kt 1.0 jets
      SystVariations.append(["SysFATJET_JER__1up"])
      SystVariations.append(["SysFATJET_JMR__1up"])
      SystVariations.append(["SysFATJET_D2R__1up"])

    # Systematics on anti-kt 1.0 jets - medium correlated NPs (choose one: this or the weakly correlated set of NPs in the analysis)
      # SystVariations.append(["SysFATJET_Medium_JET_Comb_Baseline_Kin__1up" ,  "SysFATJET_Medium_JET_Comb_Baseline_Kin__1down"])
      # SystVariations.append(["SysFATJET_Medium_JET_Comb_Modelling_Kin__1up" , "SysFATJET_Medium_JET_Comb_Modelling_Kin__1down"])
      # SystVariations.append(["SysFATJET_Medium_JET_Comb_TotalStat_Kin__1up",  "SysFATJET_Medium_JET_Comb_TotalStat_Kin__1down"])
      # SystVariations.append(["SysFATJET_Medium_JET_Comb_Tracking_Kin__1up",   "SysFATJET_Medium_JET_Comb_Tracking_Kin__1down"])

    # Systematics on anti-kt 1.0 jets - weakly correlated NPs (choose one: this or the medium correlated set of NPs in the analysis)
      SystVariations.append(["SysFATJET_Weak_JET_Comb_Baseline_mass__1up" ,     "SysFATJET_Weak_JET_Comb_Baseline_mass__1down"])
      SystVariations.append(["SysFATJET_Weak_JET_Comb_Modelling_mass__1up" ,    "SysFATJET_Weak_JET_Comb_Modelling_mass__1down"])
      SystVariations.append(["SysFATJET_Weak_JET_Comb_TotalStat_mass__1up",     "SysFATJET_Weak_JET_Comb_TotalStat_mass__1down"])
      SystVariations.append(["SysFATJET_Weak_JET_Comb_Tracking_mass__1up",      "SysFATJET_Weak_JET_Comb_Tracking_mass__1down"])

      SystVariations.append(["SysFATJET_Weak_JET_Rtrk_Baseline_pT__1up" ,       "SysFATJET_Weak_JET_Rtrk_Baseline_pT__1down"])
      SystVariations.append(["SysFATJET_Weak_JET_Rtrk_Modelling_pT__1up",       "SysFATJET_Weak_JET_Rtrk_Modelling_pT__1down"])
      SystVariations.append(["SysFATJET_Weak_JET_Rtrk_TotalStat_pT__1up" ,      "SysFATJET_Weak_JET_Rtrk_TotalStat_pT__1down"])
      SystVariations.append(["SysFATJET_Weak_JET_Rtrk_Tracking_pT__1up",        "SysFATJET_Weak_JET_Rtrk_Tracking_pT__1down"])

      SystVariations.append(["SysFATJET_Weak_JET_Rtrk_Baseline_D2Beta1__1up" ,  "SysFATJET_Weak_JET_Rtrk_Baseline_D2Beta1__1down"])
      SystVariations.append(["SysFATJET_Weak_JET_Rtrk_Modelling_D2Beta1__1up",  "SysFATJET_Weak_JET_Rtrk_Modelling_D2Beta1__1down"])
      SystVariations.append(["SysFATJET_Weak_JET_Rtrk_TotalStat_D2Beta1__1up" , "SysFATJET_Weak_JET_Rtrk_TotalStat_D2Beta1__1down"])
      SystVariations.append(["SysFATJET_Weak_JET_Rtrk_Tracking_D2Beta1__1up",   "SysFATJET_Weak_JET_Rtrk_Tracking_D2Beta1__1down"])


   # Modelling systematic Z+jets samples
      SystVariations.append(["SysZPtV__1up","SysZPtV__1down"])
      SystVariations.append(["SysZMbb__1up","SysZMbb__1down"])
      SystVariations.append(["SysMbbZShapeOnly__1up","SysMbbZShapeOnly__1down"])
    # Modelling systematic W+jets samples
      SystVariations.append(["SysWPtV__1up","SysWPtV__1down"])
      SystVariations.append(["SysWMbb__1up","SysWMbb__1down"])
      SystVariations.append(["SysMbbWShapeOnly__1up","SysMbbWShapeOnly__1down"])
    # Modelling systematic ttbar samples
      SystVariations.append(["SysTTbarPTV__1up","SysTTbarPTV__1down"])
      SystVariations.append(["SysTTbarMBB__1up","SysTTbarMBB__1down"])
      SystVariations.append(["SysMbbTTbarShapeOnly__1up","SysMbbTTbarShapeOnly__1down"])
    # Modelling systematic single top samples
      SystVariations.append(["SysStoptPTV__1up","SysStoptPTV__1down"])
      SystVariations.append(["SysStoptMBB__1up","SysStoptMBB__1down"])
      SystVariations.append(["SysMbbStoptShapeOnly__1up","SysMbbStoptShapeOnly__1down"])
    # Modelling systematic single top samples
      SystVariations.append(["SysStopWtPTV__1up","SysStopWtPTV__1down"])
      SystVariations.append(["SysStopWtMBB__1up","SysStopWtMBB__1down"])
      SystVariations.append(["SysMbbStopWtShapeOnly__1up","SysMbbStopWtShapeOnly__1down"])
    # Modelling systematic on diboson samples
      SystVariations.append(["SysVVPTVME__1up","SysVVPTVME__1down"])
      SystVariations.append(["SysVVMbbME__1up","SysVVMbbME__1down"])
      SystVariations.append(["SysMbbVVMEShapeOnly__1up","SysMbbVVMEShapeOnly__1down"])
    # Modelling systematic on diboson samples
      SystVariations.append(["SysVVPTVPSUE__1up","SysVVPTVPSUE__1down"])
      SystVariations.append(["SysVVMbbPSUE__1up","SysVVMbbPSUE__1down"])
      SystVariations.append(["SysMbbVVPSUEShapeOnly__1up","SysMbbVVPSUEShapeOnly__1down"])



    # Systematics on b-tagging
      jetCollection = ""
      if("1pfat" in region):
        jetCollection = "AntiKt2PV0TrackJets"
      else:
        jetCollection = "AntiKt4EMTopoJets"
      SystVariations.append(["SysFT_EFF_Eigen_Light_0_"+jetCollection+"__1up"     ,"SysFT_EFF_Eigen_Light_0_"+jetCollection+"__1down"])
      SystVariations.append(["SysFT_EFF_Eigen_Light_1_"+jetCollection+"__1up"     ,"SysFT_EFF_Eigen_Light_1_"+jetCollection+"__1down"])
      SystVariations.append(["SysFT_EFF_Eigen_Light_2_"+jetCollection+"__1up"     ,"SysFT_EFF_Eigen_Light_2_"+jetCollection+"__1down"])
      SystVariations.append(["SysFT_EFF_Eigen_Light_3_"+jetCollection+"__1up"     ,"SysFT_EFF_Eigen_Light_3_"+jetCollection+"__1down"])
      SystVariations.append(["SysFT_EFF_Eigen_Light_4_"+jetCollection+"__1up"     ,"SysFT_EFF_Eigen_Light_4_"+jetCollection+"__1down"])
      SystVariations.append(["SysFT_EFF_Eigen_C_0_"+jetCollection+"__1up"         ,"SysFT_EFF_Eigen_C_0_"+jetCollection+"__1down"])
      SystVariations.append(["SysFT_EFF_Eigen_C_1_"+jetCollection+"__1up"         ,"SysFT_EFF_Eigen_C_1_"+jetCollection+"__1down"])
      SystVariations.append(["SysFT_EFF_Eigen_C_2_"+jetCollection+"__1up"         ,"SysFT_EFF_Eigen_C_2_"+jetCollection+"__1down"])
      SystVariations.append(["SysFT_EFF_Eigen_C_3_"+jetCollection+"__1up"         ,"SysFT_EFF_Eigen_C_3_"+jetCollection+"__1down"])
      SystVariations.append(["SysFT_EFF_Eigen_B_0_"+jetCollection+"__1up"         ,"SysFT_EFF_Eigen_B_0_"+jetCollection+"__1down"])
      SystVariations.append(["SysFT_EFF_Eigen_B_1_"+jetCollection+"__1up"         ,"SysFT_EFF_Eigen_B_1_"+jetCollection+"__1down"])
      SystVariations.append(["SysFT_EFF_Eigen_B_2_"+jetCollection+"__1up"         ,"SysFT_EFF_Eigen_B_2_"+jetCollection+"__1down"])
      SystVariations.append(["SysFT_EFF_extrapolation_"+jetCollection+"__1up"     ,"SysFT_EFF_extrapolation_"+jetCollection+"__1down"])
      SystVariations.append(["SysFT_EFF_extrapolation_from_charm_"+jetCollection+"__1up"         ,"SysFT_EFF_extrapolation_from_charm_"+jetCollection+"__1down"])


      # shape systematic variations on multijet estimation
      if options.lepton == '0lep':
        SystVariations.append(["SysMJ_Shape0mergedMassPass__1up","SysMJ_Shape0mergedMassPass__1down"])
        SystVariations.append(["SysMJ_Shape1mergedMassPass__1up","SysMJ_Shape1mergedMassPass__1down"])
        SystVariations.append(["SysMJ_Shape2mergedMassPass__1up","SysMJ_Shape2mergedMassPass__1down"])
        SystVariations.append(["SysMJ_Shape0mergedMassFail__1up","SysMJ_Shape0mergedMassFail__1down"])
        SystVariations.append(["SysMJ_Shape1mergedMassFail__1up","SysMJ_Shape1mergedMassFail__1down"])
        SystVariations.append(["SysMJ_Shape2mergedMassFail__1up","SysMJ_Shape2mergedMassFail__1down"])
        SystVariations.append(["SysMJ_Shape0resolvedMassPass__1up","SysMJ_Shape0resolvedMassPass__1down"])
        SystVariations.append(["SysMJ_Shape1resolvedMassPass__1up","SysMJ_Shape1resolvedMassPass__1down"])
        SystVariations.append(["SysMJ_Shape2resolvedMassPass__1up","SysMJ_Shape2resolvedMassPass__1down"])
        SystVariations.append(["SysMJ_Shape0resolvedMassFail__1up","SysMJ_Shape0resolvedMassFail__1down"])
        SystVariations.append(["SysMJ_Shape1resolvedMassFail__1up","SysMJ_Shape1resolvedMassFail__1down"])
        SystVariations.append(["SysMJ_Shape2resolvedMassFail__1up","SysMJ_Shape2resolvedMassFail__1down"])


      # signal theory uncertainties
      if nLeptons == '0lep':
        SystVariations.append(["SysPdf__1up","SysPdf__1down"])
        SystVariations.append(["SysTheoryAlphaScale__1up","SysTheoryAlphaScale__1down"])
        SystVariations.append(["SysTheoryTuning__1up","SysTheoryTuning__1down"])

      

      for SystVariation in SystVariations:

        variationtype = SystVariation[0].split("__")[0]
        if len(SystVariation)==1:
          nvar    = 1
          syst_up = SystVariation[0]
          syst_do = SystVariation[0]
        if len(SystVariation)==2:
          nvar    = 2
          syst_up = SystVariation[0]
          syst_do = SystVariation[1]


        # set signal names
        if nLeptons == '0lep':
          name_sig1  = basename_sig1
          name_sig2  = basename_sig2


        print(variationtype, variable, rebin, xmin, xmax, ymax)
        suffix = region+"_"+variable
        print(suffix)

        print("Get Nominal")
        h_bkgn_zbb     = GetHist1D(f, "zbb",     "", name_zbb+"_"+suffix,    name_tt+"_"+suffix,debugmode)
        h_bkgn_zbc     = GetHist1D(f, "zbc",     "", name_zbc+"_"+suffix,    name_tt+"_"+suffix,debugmode)
        h_bkgn_zbl     = GetHist1D(f, "zbl",     "", name_zbl+"_"+suffix,    name_tt+"_"+suffix,debugmode)
        h_bkgn_zcc     = GetHist1D(f, "zcc",     "", name_zcc+"_"+suffix,    name_tt+"_"+suffix,debugmode)
        h_bkgn_zcl     = GetHist1D(f, "zcl",     "", name_zcl+"_"+suffix,    name_tt+"_"+suffix,debugmode)
        h_bkgn_zll     = GetHist1D(f, "zll",     "", name_zl+"_"+suffix,     name_tt+"_"+suffix,debugmode)
        h_bkgn_wbb     = GetHist1D(f, "wbb",     "", name_wbb+"_"+suffix,   name_tt+"_"+suffix,debugmode)
        h_bkgn_wbc     = GetHist1D(f, "wbc",     "", name_wbc+"_"+suffix,   name_tt+"_"+suffix,debugmode)
        h_bkgn_wbl     = GetHist1D(f, "wbl",     "", name_wbl+"_"+suffix,   name_tt+"_"+suffix,debugmode)
        h_bkgn_wcc     = GetHist1D(f, "wcc",     "", name_wcc+"_"+suffix,   name_tt+"_"+suffix,debugmode)
        h_bkgn_wcl     = GetHist1D(f, "wcl",     "", name_wcl+"_"+suffix,   name_tt+"_"+suffix,debugmode)
        h_bkgn_wll     = GetHist1D(f, "wll",     "", name_wl+"_"+suffix,    name_tt+"_"+suffix,debugmode)
        h_bkgn_tt      = GetHist1D(f, "tt",      "", name_tt+"_"+suffix,     name_tt+"_"+suffix,debugmode)
        h_bkgn_stop_s  = GetHist1D(f, "stop_s",  "", name_stops+"_"+suffix,  name_tt+"_"+suffix,debugmode)
        h_bkgn_stop_t  = GetHist1D(f, "stop_t",  "", name_stopt+"_"+suffix,  name_tt+"_"+suffix,debugmode)
        h_bkgn_stop_Wt = GetHist1D(f, "stop_Wt", "", name_stopwt+"_"+suffix, name_tt+"_"+suffix,debugmode)
        h_bkgn_ww      = GetHist1D(f, "ww",      "", name_ww+"_"+suffix,     name_tt+"_"+suffix,debugmode)
        h_bkgn_wz      = GetHist1D(f, "wz",      "", name_wz+"_"+suffix,     name_tt+"_"+suffix,debugmode)
        h_bkgn_zz      = GetHist1D(f, "zz",      "", name_zz+"_"+suffix,     name_tt+"_"+suffix,debugmode)

        # signals
        if nLeptons == '0lep':
          if is_dmVVhad_sig1:
            h_sig1         = GetHist1D(f, "sig1",    "", name_sig1.replace("dmVVhad", "dmVWhad")+"_"+suffix,   name_tt+"_"+suffix,debugmode)
            h_sig1_add     = GetHist1D(f, "sig1_add",    "", name_sig1.replace("dmVVhad", "dmVZhad")+"_"+suffix,   name_tt+"_"+suffix,debugmode)
            h_sig1.Add(h_sig1_add)
          else:
            h_sig1         = GetHist1D(f, "sig1",    "", name_sig1+"_"+suffix,   name_tt+"_"+suffix,debugmode)
          if is_dmVVhad_sig2:
            h_sig2         = GetHist1D(f, "sig2",    "", name_sig2.replace("dmVVhad", "dmVWhad")+"_"+suffix,   name_tt+"_"+suffix,debugmode)
            h_sig2_add         = GetHist1D(f, "sig2_add",    "", name_sig2.replace("dmVVhad", "dmVZhad")+"_"+suffix,   name_tt+"_"+suffix,debugmode)
            h_sig2.Add(h_sig2_add)
          else:
            h_sig2         = GetHist1D(f, "sig2",    "", name_sig2+"_"+suffix,   name_tt+"_"+suffix,debugmode)

        # multijet backgrounds
        if nLeptons == '0lep':
          h_bkgn_mj0tagmergedmasspass    = GetHist1D(f, "mj0tagmergedmasspass",      "", name_mj0tagmergedmasspass+"_"+suffix,     name_tt+"_"+suffix,debugmode)
          h_bkgn_mj1tagmergedmasspass    = GetHist1D(f, "mj1tagmergedmasspass",      "", name_mj1tagmergedmasspass+"_"+suffix,     name_tt+"_"+suffix,debugmode)
          h_bkgn_mj2tagmergedmasspass    = GetHist1D(f, "mj2tagmergedmasspass",      "", name_mj2tagmergedmasspass+"_"+suffix,     name_tt+"_"+suffix,debugmode)
          h_bkgn_mj0tagmergedmassfail    = GetHist1D(f, "mj0tagmergedmassfail",      "", name_mj0tagmergedmassfail+"_"+suffix,     name_tt+"_"+suffix,debugmode)
          h_bkgn_mj1tagmergedmassfail    = GetHist1D(f, "mj1tagmergedmassfail",      "", name_mj1tagmergedmassfail+"_"+suffix,     name_tt+"_"+suffix,debugmode)
          h_bkgn_mj2tagmergedmassfail    = GetHist1D(f, "mj2tagmergedmassfail",      "", name_mj2tagmergedmassfail+"_"+suffix,     name_tt+"_"+suffix,debugmode)
          h_bkgn_mj0tagresolvedmasspass    = GetHist1D(f, "mj0tagresolvedmasspass",      "", name_mj0tagresolvedmasspass+"_"+suffix,     name_tt+"_"+suffix,debugmode)
          h_bkgn_mj1tagresolvedmasspass    = GetHist1D(f, "mj1tagresolvedmasspass",      "", name_mj1tagresolvedmasspass+"_"+suffix,     name_tt+"_"+suffix,debugmode)
          h_bkgn_mj2tagresolvedmasspass    = GetHist1D(f, "mj2tagresolvedmasspass",      "", name_mj2tagresolvedmasspass+"_"+suffix,     name_tt+"_"+suffix,debugmode)
          h_bkgn_mj0tagresolvedmassfail    = GetHist1D(f, "mj0tagresolvedmassfail",      "", name_mj0tagresolvedmassfail+"_"+suffix,     name_tt+"_"+suffix,debugmode)
          h_bkgn_mj1tagresolvedmassfail    = GetHist1D(f, "mj1tagresolvedmassfail",      "", name_mj1tagresolvedmassfail+"_"+suffix,     name_tt+"_"+suffix,debugmode)
          h_bkgn_mj2tagresolvedmassfail    = GetHist1D(f, "mj2tagresolvedmassfail",      "", name_mj2tagresolvedmassfail+"_"+suffix,     name_tt+"_"+suffix,debugmode)


        c = TCanvas("c","c",500,500)

        isSignalTheorySystematic = False
        hist_array = []
        if variationtype == "SysZPtV" or variationtype == "SysZMbb" or variationtype == "SysMbbZShapeOnly":
          hist_array = [h_bkgn_zbb,h_bkgn_zbc,h_bkgn_zbl,h_bkgn_zcc,h_bkgn_zcl,h_bkgn_zll]
        elif variationtype == "SysWPtV" or variationtype == "SysWMbb" or variationtype == "SysMbbWShapeOnly":
          hist_array = [h_bkgn_wbb,h_bkgn_wbc,h_bkgn_wbl,h_bkgn_wcc,h_bkgn_wcl,h_bkgn_wll]
        elif variationtype == "SysTTbarPTV" or variationtype == "SysTTbarMBB" or variationtype == "SysNNLORW" or variationtype == "SysMbbTTbarShapeOnly":
          hist_array = [h_bkgn_tt]
        elif variationtype == "SysStoptPTV" or variationtype == "SysStoptMBB" or variationtype == "SysStopWtPTV" or variationtype == "SysStopWtMBB" or variationtype == "SysMbbStoptShapeOnly" or variationtype == "SysMbbStopWtShapeOnly":
          hist_array = [h_bkgn_stop_s, h_bkgn_stop_t, h_bkgn_stop_Wt]
        elif variationtype == "SysVVPTVME" or variationtype == "SysVVMbbME" or variationtype == "SysVVPTVPSUE" or variationtype == "SysVVMbbPSUE" or variationtype == "SysMbbVVMEShapeOnly" or variationtype == "SysMbbVVPSUEShapeOnly":
          hist_array = [h_bkgn_ww, h_bkgn_wz, h_bkgn_zz]
        elif variationtype == "SysMJ_Shape0mergedMassPass":
          hist_array = [h_bkgn_mj0tagmergedmasspass]
        elif variationtype == "SysMJ_Shape1mergedMassPass":
          hist_array = [h_bkgn_mj1tagmergedmasspass]
        elif variationtype == "SysMJ_Shape2mergedMassPass":
          hist_array = [h_bkgn_mj2tagmergedmasspass]
        elif variationtype == "SysMJ_Shape0mergedMassFail":
          hist_array = [h_bkgn_mj0tagmergedmassfail]
        elif variationtype == "SysMJ_Shape1mergedMassFail":
          hist_array = [h_bkgn_mj1tagmergedmassfail]
        elif variationtype == "SysMJ_Shape2mergedMassFail":
          hist_array = [h_bkgn_mj2tagmergedmassfail]
        elif variationtype == "SysMJ_Shape0resolvedMassPass":
          hist_array = [h_bkgn_mj0tagresolvedmasspass]
        elif variationtype == "SysMJ_Shape1resolvedMassPass":
          hist_array = [h_bkgn_mj1tagresolvedmasspass]
        elif variationtype == "SysMJ_Shape2resolvedMassPass":
          hist_array = [h_bkgn_mj2tagresolvedmasspass]
        elif variationtype == "SysMJ_Shape0resolvedMassFail":
          hist_array = [h_bkgn_mj0tagresolvedmassfail]
        elif variationtype == "SysMJ_Shape1resolvedMassFail":
          hist_array = [h_bkgn_mj1tagresolvedmassfail]
        elif variationtype == "SysMJ_Shape2resolvedMassFail":
          hist_array = [h_bkgn_mj2tagresolvedmassfail]        
        else:
          hist_array = [h_bkgn_zbb,h_bkgn_zbc,h_bkgn_zbl,h_bkgn_zcc,h_bkgn_zcl,h_bkgn_zll, h_bkgn_wbb,h_bkgn_wbc,h_bkgn_wbl,h_bkgn_wcc,h_bkgn_wcl,h_bkgn_wll, h_bkgn_tt, h_bkgn_stop_s, h_bkgn_stop_t, h_bkgn_stop_Wt, h_bkgn_ww, h_bkgn_wz, h_bkgn_zz]
        h_bkgn = MakeHistSum("bkgn", hist_array)

        if nLeptons == '0lep':
          h_sig1.Rebin(rebin)
          h_sig2.Rebin(rebin)
        h_bkgn.Rebin(rebin)

        if nLeptons == '0lep':
          h_sig1.SetLineColor(1)
          h_sig2.SetLineColor(1)
        h_bkgn.SetLineColor(1)

        if nLeptons == '0lep':
          h_sig1.SetLineWidth(3)
          h_sig2.SetLineWidth(3)
        h_bkgn.SetLineWidth(3)

        print("Systematic Variation: {}".format(variationtype))
        print("Get 1 Up")
        h_bkgn_zbb_up     = GetHist1D(f, "zbb_up",     "Systematics", name_zbb+"_"+suffix+"_"+syst_up,    name_tt+"_"+suffix,debugmode)
        h_bkgn_zbc_up     = GetHist1D(f, "zbc_up",     "Systematics", name_zbc+"_"+suffix+"_"+syst_up,    name_tt+"_"+suffix,debugmode)
        h_bkgn_zbl_up     = GetHist1D(f, "zbl_up",     "Systematics", name_zbl+"_"+suffix+"_"+syst_up,    name_tt+"_"+suffix,debugmode)
        h_bkgn_zcc_up     = GetHist1D(f, "zcc_up",     "Systematics", name_zcc+"_"+suffix+"_"+syst_up,    name_tt+"_"+suffix,debugmode)
        h_bkgn_zcl_up     = GetHist1D(f, "zcl_up",     "Systematics", name_zcl+"_"+suffix+"_"+syst_up,    name_tt+"_"+suffix,debugmode)
        h_bkgn_zll_up     = GetHist1D(f, "zll_up",     "Systematics", name_zl+"_"+suffix+"_"+syst_up,      name_tt+"_"+suffix,debugmode)
        h_bkgn_wbb_up     = GetHist1D(f, "wbb_up",     "Systematics", name_wbb+"_"+suffix+"_"+syst_up,      name_tt+"_"+suffix,debugmode)
        h_bkgn_wbc_up     = GetHist1D(f, "wbc_up",     "Systematics", name_wbc+"_"+suffix+"_"+syst_up,      name_tt+"_"+suffix,debugmode)
        h_bkgn_wbl_up     = GetHist1D(f, "wbl_up",     "Systematics", name_wbl+"_"+suffix+"_"+syst_up,      name_tt+"_"+suffix,debugmode)
        h_bkgn_wcc_up     = GetHist1D(f, "wcc_up",     "Systematics", name_wcc+"_"+suffix+"_"+syst_up,      name_tt+"_"+suffix,debugmode)
        h_bkgn_wcl_up     = GetHist1D(f, "wcl_up",     "Systematics", name_wcl+"_"+suffix+"_"+syst_up,      name_tt+"_"+suffix,debugmode)
        h_bkgn_wll_up     = GetHist1D(f, "wll_up",     "Systematics", name_wl+"_"+suffix+"_"+syst_up,      name_tt+"_"+suffix,debugmode)
        h_bkgn_tt_up      = GetHist1D(f, "tt_up",      "Systematics", name_tt+"_"+suffix+"_"+syst_up,     name_tt+"_"+suffix,debugmode)
        h_bkgn_stop_s_up  = GetHist1D(f, "stop_s_up",  "Systematics", name_stops+"_"+suffix+"_"+syst_up,  name_tt+"_"+suffix,debugmode)
        h_bkgn_stop_t_up  = GetHist1D(f, "stop_t_up",  "Systematics", name_stopt+"_"+suffix+"_"+syst_up,  name_tt+"_"+suffix,debugmode)
        h_bkgn_stop_Wt_up = GetHist1D(f, "stop_Wt_up", "Systematics", name_stopwt+"_"+suffix+"_"+syst_up, name_tt+"_"+suffix,debugmode)
        h_bkgn_ww_up      = GetHist1D(f, "ww_up",      "Systematics", name_ww+"_"+suffix+"_"+syst_up,     name_tt+"_"+suffix,debugmode)
        h_bkgn_wz_up      = GetHist1D(f, "wz_up",      "Systematics", name_wz+"_"+suffix+"_"+syst_up,     name_tt+"_"+suffix,debugmode)
        h_bkgn_zz_up      = GetHist1D(f, "zz_up",      "Systematics", name_zz+"_"+suffix+"_"+syst_up,     name_tt+"_"+suffix,debugmode)

        # signals
        if nLeptons == '0lep':
          if is_dmVVhad_sig1:
            h_sig1_up         = GetHist1D(f, "sig1_up",    "Systematics", name_sig1.replace("dmVVhad", "dmVWhad")+"_"+suffix+"_"+syst_up,   name_tt+"_"+suffix,debugmode)
            h_sig1_up_add     = GetHist1D(f, "sig1_up_add",    "Systematics", name_sig1.replace("dmVVhad", "dmVZhad")+"_"+suffix+"_"+syst_up,   name_tt+"_"+suffix,debugmode)
            h_sig1_up.Add(h_sig1_up_add)
          else:
            h_sig1_up         = GetHist1D(f, "sig1_up",    "Systematics", name_sig1+"_"+suffix+"_"+syst_up,   name_tt+"_"+suffix,debugmode)
          if is_dmVVhad_sig2:
            h_sig2_up         = GetHist1D(f, "sig2_up",    "Systematics", name_sig2.replace("dmVVhad", "dmVWhad")+"_"+suffix+"_"+syst_up,   name_tt+"_"+suffix,debugmode)
            h_sig2_up_add     = GetHist1D(f, "sig2_up_add",    "Systematics", name_sig2.replace("dmVVhad", "dmVZhad")+"_"+suffix+"_"+syst_up,   name_tt+"_"+suffix,debugmode)
            h_sig2_up.Add(h_sig2_up_add)
          else:
            h_sig2_up         = GetHist1D(f, "sig2_up",    "Systematics", name_sig2+"_"+suffix+"_"+syst_up,   name_tt+"_"+suffix,debugmode)
        
        # multijet backgrounds - up variation
        if nLeptons == '0lep':
          h_bkgn_mj0tagmergedmasspass_up    = GetHist1D(f, "mj0tagmergedmasspass_up",      "Systematics", name_mj0tagmergedmasspass+"_"+suffix+"_"+syst_up,     name_tt+"_"+suffix,debugmode)
          h_bkgn_mj1tagmergedmasspass_up    = GetHist1D(f, "mj1tagmergedmasspass_up",      "Systematics", name_mj1tagmergedmasspass+"_"+suffix+"_"+syst_up,     name_tt+"_"+suffix,debugmode)
          h_bkgn_mj2tagmergedmasspass_up    = GetHist1D(f, "mj2tagmergedmasspass_up",      "Systematics", name_mj2tagmergedmasspass+"_"+suffix+"_"+syst_up,     name_tt+"_"+suffix,debugmode)
          h_bkgn_mj0tagmergedmassfail_up    = GetHist1D(f, "mj0tagmergedmassfail_up",      "Systematics", name_mj0tagmergedmassfail+"_"+suffix+"_"+syst_up,     name_tt+"_"+suffix,debugmode)
          h_bkgn_mj1tagmergedmassfail_up    = GetHist1D(f, "mj1tagmergedmassfail_up",      "Systematics", name_mj1tagmergedmassfail+"_"+suffix+"_"+syst_up,     name_tt+"_"+suffix,debugmode)
          h_bkgn_mj2tagmergedmassfail_up    = GetHist1D(f, "mj2tagmergedmassfail_up",      "Systematics", name_mj2tagmergedmassfail+"_"+suffix+"_"+syst_up,     name_tt+"_"+suffix,debugmode)
          h_bkgn_mj0tagresolvedmasspass_up    = GetHist1D(f, "mj0tagresolvedmasspass_up",      "Systematics", name_mj0tagresolvedmasspass+"_"+suffix+"_"+syst_up,     name_tt+"_"+suffix,debugmode)
          h_bkgn_mj1tagresolvedmasspass_up    = GetHist1D(f, "mj1tagresolvedmasspass_up",      "Systematics", name_mj1tagresolvedmasspass+"_"+suffix+"_"+syst_up,     name_tt+"_"+suffix,debugmode)
          h_bkgn_mj2tagresolvedmasspass_up    = GetHist1D(f, "mj2tagresolvedmasspass_up",      "Systematics", name_mj2tagresolvedmasspass+"_"+suffix+"_"+syst_up,     name_tt+"_"+suffix,debugmode)
          h_bkgn_mj0tagresolvedmassfail_up    = GetHist1D(f, "mj0tagresolvedmassfail_up",      "Systematics", name_mj0tagresolvedmassfail+"_"+suffix+"_"+syst_up,     name_tt+"_"+suffix,debugmode)
          h_bkgn_mj1tagresolvedmassfail_up    = GetHist1D(f, "mj1tagresolvedmassfail_up",      "Systematics", name_mj1tagresolvedmassfail+"_"+suffix+"_"+syst_up,     name_tt+"_"+suffix,debugmode)
          h_bkgn_mj2tagresolvedmassfail_up    = GetHist1D(f, "mj2tagresolvedmassfail_up",      "Systematics", name_mj2tagresolvedmassfail+"_"+suffix+"_"+syst_up,     name_tt+"_"+suffix,debugmode)



        if variationtype == "SysZPtV" or variationtype == "SysZMbb" or variationtype == "SysMbbZShapeOnly":
          hist_array = [h_bkgn_zbb_up,h_bkgn_zbc_up,h_bkgn_zbl_up,h_bkgn_zcc_up,h_bkgn_zcl_up,h_bkgn_zll_up]
        elif variationtype == "SysWPtV" or variationtype == "SysWMbb" or variationtype == "SysMbbWShapeOnly":
          hist_array = [h_bkgn_wbb_up,h_bkgn_wbc_up,h_bkgn_wbl_up,h_bkgn_wcc_up,h_bkgn_wcl_up,h_bkgn_wll_up]
        elif variationtype == "SysTTbarPTV" or variationtype == "SysTTbarMBB" or variationtype == "SysNNLORW" or variationtype == "SysMbbTTbarShapeOnly":
          hist_array = [h_bkgn_tt_up]
        elif variationtype == "SysStoptPTV" or variationtype == "SysStoptMBB" or variationtype == "SysStopWtPTV" or variationtype == "SysStopWtMBB" or variationtype == "SysMbbStoptShapeOnly" or variationtype == "SysMbbStopWtShapeOnly":
          hist_array = [h_bkgn_stop_s_up, h_bkgn_stop_t_up, h_bkgn_stop_Wt_up]
        elif variationtype == "SysVVPTVME" or variationtype == "SysVVMbbME" or variationtype == "SysVVPTVPSUE" or variationtype == "SysVVMbbPSUE" or variationtype == "SysMbbVVMEShapeOnly" or variationtype == "SysMbbVVPSUEShapeOnly":
          hist_array = [h_bkgn_ww_up ,h_bkgn_wz_up, h_bkgn_zz_up]
        elif variationtype == "SysMJ_Shape0mergedMassPass":
          hist_array = [h_bkgn_mj0tagmergedmasspass_up]
        elif variationtype == "SysMJ_Shape1mergedMassPass":
          hist_array = [h_bkgn_mj1tagmergedmasspass_up]
        elif variationtype == "SysMJ_Shape2mergedMassPass":
          hist_array = [h_bkgn_mj2tagmergedmasspass_up]
        elif variationtype == "SysMJ_Shape0mergedMassFail":
          hist_array = [h_bkgn_mj0tagmergedmassfail_up]
        elif variationtype == "SysMJ_Shape1mergedMassFail":
          hist_array = [h_bkgn_mj1tagmergedmassfail_up]
        elif variationtype == "SysMJ_Shape2mergedMassFail":
          hist_array = [h_bkgn_mj2tagmergedmassfail_up]
        elif variationtype == "SysMJ_Shape0resolvedMassPass":
          hist_array = [h_bkgn_mj0tagresolvedmasspass_up]
        elif variationtype == "SysMJ_Shape1resolvedMassPass":
          hist_array = [h_bkgn_mj1tagresolvedmasspass_up]
        elif variationtype == "SysMJ_Shape2resolvedMassPass":
          hist_array = [h_bkgn_mj2tagresolvedmasspass_up]
        elif variationtype == "SysMJ_Shape0resolvedMassFail":
          hist_array = [h_bkgn_mj0tagresolvedmassfail_up]
        elif variationtype == "SysMJ_Shape1resolvedMassFail":
          hist_array = [h_bkgn_mj1tagresolvedmassfail_up]
        elif variationtype == "SysMJ_Shape2resolvedMassFail":
          hist_array = [h_bkgn_mj2tagresolvedmassfail_up]
        else:
          hist_array = [h_bkgn_zbb_up,h_bkgn_zbc_up,h_bkgn_zbl_up,h_bkgn_zcc_up,h_bkgn_zcl_up,h_bkgn_zll_up, h_bkgn_wbb_up,h_bkgn_wbc_up,h_bkgn_wbl_up,h_bkgn_wcc_up,h_bkgn_wcl_up,h_bkgn_wll_up, h_bkgn_tt_up, h_bkgn_stop_s_up, h_bkgn_stop_t_up, h_bkgn_stop_Wt_up, h_bkgn_ww_up, h_bkgn_wz_up, h_bkgn_zz_up]

        h_bkgn_up = MakeHistSum("bkgn_up", hist_array)

        if nLeptons == '0lep':
          h_sig1_up.Rebin(rebin)
          h_sig2_up.Rebin(rebin)
        h_bkgn_up.Rebin(rebin)

        if nLeptons == '0lep':
          h_sig1_up.SetLineColor(4)
          h_sig2_up.SetLineColor(4)
        h_bkgn_up.SetLineColor(4)

        if nLeptons == '0lep':
          h_sig1_up.SetLineWidth(2)
          h_sig2_up.SetLineWidth(2)
        h_bkgn_up.SetLineWidth(2)


        print("Get 1 Down")
        h_bkgn_zbb_do     = GetHist1D(f, "zbb_do",     "Systematics", name_zbb+"_"+suffix+"_"+syst_do,    name_tt+"_"+suffix,debugmode)
        h_bkgn_zbc_do     = GetHist1D(f, "zbc_do",     "Systematics", name_zbc+"_"+suffix+"_"+syst_do,    name_tt+"_"+suffix,debugmode)
        h_bkgn_zbl_do     = GetHist1D(f, "zbl_do",     "Systematics", name_zbl+"_"+suffix+"_"+syst_do,    name_tt+"_"+suffix,debugmode)
        h_bkgn_zcc_do     = GetHist1D(f, "zcc_do",     "Systematics", name_zcc+"_"+suffix+"_"+syst_do,    name_tt+"_"+suffix,debugmode)
        h_bkgn_zcl_do     = GetHist1D(f, "zcl_do",     "Systematics", name_zcl+"_"+suffix+"_"+syst_do,    name_tt+"_"+suffix,debugmode)
        h_bkgn_zll_do     = GetHist1D(f, "zll_do",     "Systematics", name_zl+"_"+suffix+"_"+syst_do,      name_tt+"_"+suffix,debugmode)
        h_bkgn_wbb_do     = GetHist1D(f, "wbb_do",     "Systematics", name_wbb+"_"+suffix+"_"+syst_do,      name_tt+"_"+suffix,debugmode)
        h_bkgn_wbc_do     = GetHist1D(f, "wbc_do",     "Systematics", name_wbc+"_"+suffix+"_"+syst_do,      name_tt+"_"+suffix,debugmode)
        h_bkgn_wbl_do     = GetHist1D(f, "wbl_do",     "Systematics", name_wbl+"_"+suffix+"_"+syst_do,      name_tt+"_"+suffix,debugmode)
        h_bkgn_wcc_do     = GetHist1D(f, "wcc_do",     "Systematics", name_wcc+"_"+suffix+"_"+syst_do,      name_tt+"_"+suffix,debugmode)
        h_bkgn_wcl_do     = GetHist1D(f, "wcl_do",     "Systematics", name_wcl+"_"+suffix+"_"+syst_do,      name_tt+"_"+suffix,debugmode)
        h_bkgn_wll_do     = GetHist1D(f, "wll_do",     "Systematics", name_wl+"_"+suffix+"_"+syst_do,      name_tt+"_"+suffix,debugmode)
        h_bkgn_tt_do      = GetHist1D(f, "tt_do",      "Systematics", name_tt+"_"+suffix+"_"+syst_do,     name_tt+"_"+suffix,debugmode)
        h_bkgn_stop_s_do  = GetHist1D(f, "stop_s_do",  "Systematics", name_stops+"_"+suffix+"_"+syst_do,  name_tt+"_"+suffix,debugmode)
        h_bkgn_stop_t_do  = GetHist1D(f, "stop_t_do",  "Systematics", name_stopt+"_"+suffix+"_"+syst_do,  name_tt+"_"+suffix,debugmode)
        h_bkgn_stop_Wt_do = GetHist1D(f, "stop_Wt_do", "Systematics", name_stopwt+"_"+suffix+"_"+syst_do, name_tt+"_"+suffix,debugmode)
        h_bkgn_ww_do      = GetHist1D(f, "ww_do",      "Systematics", name_ww+"_"+suffix+"_"+syst_do,     name_tt+"_"+suffix,debugmode)
        h_bkgn_wz_do      = GetHist1D(f, "wz_do",      "Systematics", name_wz+"_"+suffix+"_"+syst_do,     name_tt+"_"+suffix,debugmode)
        h_bkgn_zz_do      = GetHist1D(f, "zz_do",      "Systematics", name_zz+"_"+suffix+"_"+syst_do,     name_tt+"_"+suffix,debugmode)



        # signals
        if nLeptons == '0lep':
          if is_dmVVhad_sig1:
            h_sig1_do         = GetHist1D(f, "sig1_do",    "Systematics", name_sig1.replace("dmVVhad", "dmVWhad")+"_"+suffix+"_"+syst_do,   name_tt+"_"+suffix,debugmode)
            h_sig1_do_add         = GetHist1D(f, "sig1_do_add",    "Systematics", name_sig1.replace("dmVVhad", "dmVZhad")+"_"+suffix+"_"+syst_do,   name_tt+"_"+suffix,debugmode)
            h_sig1_do.Add(h_sig1_do_add)
          else:
            h_sig1_do         = GetHist1D(f, "sig1_do",    "Systematics", name_sig1+"_"+suffix+"_"+syst_do,   name_tt+"_"+suffix,debugmode)
          if is_dmVVhad_sig2:
            h_sig2_do         = GetHist1D(f, "sig2_do",    "Systematics", name_sig2.replace("dmVVhad", "dmVWhad")+"_"+suffix+"_"+syst_do,   name_tt+"_"+suffix,debugmode)
            h_sig2_do_add         = GetHist1D(f, "sig2_do_add",    "Systematics", name_sig2.replace("dmVVhad", "dmVZhad")+"_"+suffix+"_"+syst_do,   name_tt+"_"+suffix,debugmode)
            h_sig2_do.Add(h_sig2_do_add)
          else:
            h_sig2_do         = GetHist1D(f, "sig2_do",    "Systematics", name_sig2+"_"+suffix+"_"+syst_do,   name_tt+"_"+suffix,debugmode)
        

        # multijet backgrounds - down variation
        if nLeptons == '0lep':
          h_bkgn_mj0tagmergedmasspass_do    = GetHist1D(f, "mj0tagmergedmasspass_do",      "Systematics", name_mj0tagmergedmasspass+"_"+suffix+"_"+syst_do,     name_tt+"_"+suffix,debugmode)
          h_bkgn_mj1tagmergedmasspass_do    = GetHist1D(f, "mj1tagmergedmasspass_do",      "Systematics", name_mj1tagmergedmasspass+"_"+suffix+"_"+syst_do,     name_tt+"_"+suffix,debugmode)
          h_bkgn_mj2tagmergedmasspass_do    = GetHist1D(f, "mj2tagmergedmasspass_do",      "Systematics", name_mj2tagmergedmasspass+"_"+suffix+"_"+syst_do,     name_tt+"_"+suffix,debugmode)
          h_bkgn_mj0tagmergedmassfail_do    = GetHist1D(f, "mj0tagmergedmassfail_do",      "Systematics", name_mj0tagmergedmassfail+"_"+suffix+"_"+syst_do,     name_tt+"_"+suffix,debugmode)
          h_bkgn_mj1tagmergedmassfail_do    = GetHist1D(f, "mj1tagmergedmassfail_do",      "Systematics", name_mj1tagmergedmassfail+"_"+suffix+"_"+syst_do,     name_tt+"_"+suffix,debugmode)
          h_bkgn_mj2tagmergedmassfail_do    = GetHist1D(f, "mj2tagmergedmassfail_do",      "Systematics", name_mj2tagmergedmassfail+"_"+suffix+"_"+syst_do,     name_tt+"_"+suffix,debugmode)
          h_bkgn_mj0tagresolvedmasspass_do    = GetHist1D(f, "mj0tagresolvedmasspass_do",      "Systematics", name_mj0tagresolvedmasspass+"_"+suffix+"_"+syst_do,     name_tt+"_"+suffix,debugmode)
          h_bkgn_mj1tagresolvedmasspass_do    = GetHist1D(f, "mj1tagresolvedmasspass_do",      "Systematics", name_mj1tagresolvedmasspass+"_"+suffix+"_"+syst_do,     name_tt+"_"+suffix,debugmode)
          h_bkgn_mj2tagresolvedmasspass_do    = GetHist1D(f, "mj2tagresolvedmasspass_do",      "Systematics", name_mj2tagresolvedmasspass+"_"+suffix+"_"+syst_do,     name_tt+"_"+suffix,debugmode)
          h_bkgn_mj0tagresolvedmassfail_do    = GetHist1D(f, "mj0tagresolvedmassfail_do",      "Systematics", name_mj0tagresolvedmassfail+"_"+suffix+"_"+syst_do,     name_tt+"_"+suffix,debugmode)
          h_bkgn_mj1tagresolvedmassfail_do    = GetHist1D(f, "mj1tagresolvedmassfail_do",      "Systematics", name_mj1tagresolvedmassfail+"_"+suffix+"_"+syst_do,     name_tt+"_"+suffix,debugmode)
          h_bkgn_mj2tagresolvedmassfail_do    = GetHist1D(f, "mj2tagresolvedmassfail_do",      "Systematics", name_mj2tagresolvedmassfail+"_"+suffix+"_"+syst_do,     name_tt+"_"+suffix,debugmode)



        hist_array = []
        if variationtype == "SysZPtV" or variationtype == "SysZMbb" or variationtype == "SysMbbZShapeOnly":
          hist_array = [h_bkgn_zbb_do,h_bkgn_zbc_do,h_bkgn_zbl_do,h_bkgn_zcc_do,h_bkgn_zcl_do,h_bkgn_zll_do]
        elif variationtype == "SysWPtV" or variationtype == "SysWMbb" or variationtype == "SysMbbWShapeOnly":
          hist_array = [h_bkgn_wbb_do,h_bkgn_wbc_do,h_bkgn_wbl_do,h_bkgn_wcc_do,h_bkgn_wcl_do,h_bkgn_wll_do]
        elif variationtype == "SysTTbarPTV" or variationtype == "SysTTbarMBB" or variationtype == "SysNNLORW" or variationtype == "SysMbbTTbarShapeOnly":
          hist_array = [h_bkgn_tt_do]
        elif variationtype == "SysStoptPTV" or variationtype == "SysStoptMBB" or variationtype == "SysStopWtPTV" or variationtype == "SysStopWtMBB" or variationtype == "SysMbbStoptShapeOnly" or variationtype == "SysMbbStopWtShapeOnly":
          hist_array = [h_bkgn_stop_s_do, h_bkgn_stop_t_do, h_bkgn_stop_Wt_do]
        elif variationtype == "SysVVPTVME" or variationtype == "SysVVMbbME" or variationtype == "SysVVPTVPSUE" or variationtype == "SysVVMbbPSUE" or variationtype == "SysMbbVVMEShapeOnly" or variationtype == "SysMbbVVPSUEShapeOnly":
          hist_array = [h_bkgn_ww_do, h_bkgn_wz_do, h_bkgn_zz_do]
        elif variationtype == "SysMJ_Shape0mergedMassPass":
          hist_array = [h_bkgn_mj0tagmergedmasspass_do]
        elif variationtype == "SysMJ_Shape1mergedMassPass":
          hist_array = [h_bkgn_mj1tagmergedmasspass_do]
        elif variationtype == "SysMJ_Shape2mergedMassPass":
          hist_array = [h_bkgn_mj2tagmergedmasspass_do]
        elif variationtype == "SysMJ_Shape0mergedMassFail":
          hist_array = [h_bkgn_mj0tagmergedmassfail_do]
        elif variationtype == "SysMJ_Shape1mergedMassFail":
          hist_array = [h_bkgn_mj1tagmergedmassfail_do]
        elif variationtype == "SysMJ_Shape2mergedMassFail":
          hist_array = [h_bkgn_mj2tagmergedmassfail_do]
        elif variationtype == "SysMJ_Shape0resolvedMassPass":
          hist_array = [h_bkgn_mj0tagresolvedmasspass_do]
        elif variationtype == "SysMJ_Shape1resolvedMassPass":
          hist_array = [h_bkgn_mj1tagresolvedmasspass_do]
        elif variationtype == "SysMJ_Shape2resolvedMassPass":
          hist_array = [h_bkgn_mj2tagresolvedmasspass_do]
        elif variationtype == "SysMJ_Shape0resolvedMassFail":
          hist_array = [h_bkgn_mj0tagresolvedmassfail_do]
        elif variationtype == "SysMJ_Shape1resolvedMassFail":
          hist_array = [h_bkgn_mj1tagresolvedmassfail_do]
        elif variationtype == "SysMJ_Shape2resolvedMassFail":
          hist_array = [h_bkgn_mj2tagresolvedmassfail_do]
        else:
          hist_array = [h_bkgn_zbb_do,h_bkgn_zbc_do,h_bkgn_zbl_do,h_bkgn_zcc_do,h_bkgn_zcl_do,h_bkgn_zll_do, h_bkgn_wbb_do,h_bkgn_wbc_do,h_bkgn_wbl_do,h_bkgn_wcc_do,h_bkgn_wcl_do,h_bkgn_wll_do, h_bkgn_tt_do, h_bkgn_stop_s_do, h_bkgn_stop_t_do, h_bkgn_stop_Wt_do, h_bkgn_ww_do, h_bkgn_wz_do, h_bkgn_zz_do]

        h_bkgn_do = MakeHistSum("bkgn_do", hist_array)

        if nLeptons == '0lep':
          h_sig1_do.Rebin(rebin)
          h_sig2_do.Rebin(rebin)
        h_bkgn_do.Rebin(rebin)

        if nLeptons == '0lep':
          h_sig1_do.SetLineColor(2)
          h_sig2_do.SetLineColor(2)
        h_bkgn_do.SetLineColor(2)

        if nLeptons == '0lep':
          h_sig1_do.SetLineWidth(2)
          h_sig2_do.SetLineWidth(2)
        h_bkgn_do.SetLineWidth(2)

        # ratio plots
        h_bkgn_ratio_up = h_bkgn_up.Clone("bkgn_ratio_up")
        if nLeptons == '0lep':
          h_sig1_ratio_up = h_sig1_up.Clone("sig1_ratio_up")
          h_sig2_ratio_up = h_sig2_up.Clone("sig2_ratio_up")
        h_bkgn_ratio_up.Divide(h_bkgn)
        if nLeptons == '0lep':
          h_sig1_ratio_up.Divide(h_sig1)
          h_sig2_ratio_up.Divide(h_sig2)

        h_bkgn_ratio_do = h_bkgn_do.Clone("bkgn_ratio_do")
        if nLeptons == '0lep':
          h_sig1_ratio_do = h_sig1_do.Clone("sig1_ratio_do")
          h_sig2_ratio_do = h_sig2_do.Clone("sig2_ratio_do")
        h_bkgn_ratio_do.Divide(h_bkgn)
        if nLeptons == '0lep':
          h_sig1_ratio_do.Divide(h_sig1)
          h_sig2_ratio_do.Divide(h_sig2)

        if nLeptons != "0lep":
          n_sig1_nom = 1
          n_sig2_nom = 1
        if nLeptons == '0lep':
          n_sig1_nom = h_sig1.Integral()
          n_sig2_nom = h_sig2.Integral()
        # get integral variations
        n_bkgn_nom = h_bkgn.Integral()

        n_bkgn_up  = h_bkgn_up.Integral()
        if nLeptons == '0lep':
          n_sig1_up  = h_sig1_up.Integral()
          n_sig2_up  = h_sig2_up.Integral()
        else:
          n_sig1_up = 1.
          n_sig2_up = 1.
        n_bkgn_do  = h_bkgn_do.Integral()
        if nLeptons == '0lep':
          n_sig1_do  = h_sig1_do.Integral()
          n_sig2_do  = h_sig2_do.Integral()
        else:
          n_sig1_do = 1.
          n_sig2_do = 1.


        # write to csv file
        # what to write in csv file:
        # region name, systematic source, background variation up, down, sig1 variation up, down, sig2 variation up, down
        # region, 
        syst = SystVariation[0].replace("__1up", "")
        region = RegionAndVariable[0]
        if syst=="SysJET_JER_SINGLE_NP" or syst=="SysMET_SoftTrk_ResoPerp" or syst=="SysMET_SoftTrk_ResoPara" or syst=="SysFATJET_JER" or syst=="SysFATJET_JMR" or syst=="SysFATJET_JD2R":
          line = "{region},{sys},{bkg_nom},{bkg_up},{bkg_do},{sig1_nom},{sig1_up},{sig1_do},{sig2_nom},{sig2_up},{sig2_do}\n".format(region=region,sys=syst,bkg_nom=n_bkgn_nom,bkg_up=n_bkgn_up,bkg_do=(2*n_bkgn_nom-n_bkgn_up),sig1_nom=n_sig1_nom,sig1_up=n_sig1_up,sig1_do=(2*n_sig1_nom-n_sig1_up),sig2_nom=n_sig2_nom,sig2_up=n_sig2_up,sig2_do=(2*n_sig2_nom-n_sig2_up))
          print(line)
          csvFile.write(line)
        else:
          line = "{region},{sys},{bkg_nom},{bkg_up},{bkg_do},{sig1_nom},{sig1_up},{sig1_do},{sig2_nom},{sig2_up},{sig2_do}\n".format(region=region,sys=syst,bkg_nom=n_bkgn_nom,bkg_up=n_bkgn_up,bkg_do=n_bkgn_do,sig1_nom=n_sig1_nom,sig1_up=n_sig1_up,sig1_do=n_sig1_do,sig2_nom=n_sig2_nom,sig2_up=n_sig2_up,sig2_do=n_sig2_do)
          print(line)
          csvFile.write(line)

        try:  
          var_bkgn_up = str(round(((n_bkgn_up/n_bkgn_nom)-1.0)*100.0 , 2))
        except Exception:
          var_bkgn_up = str(0.0)
        try:
          var_bkgn_do = str(round(((n_bkgn_do/n_bkgn_nom)-1.0)*100.0 , 2))
        except Exception:
          var_bkgn_do = str(0.0)
        if nLeptons == '0lep':
          try:
            var_sig1_up = str(round(((n_sig1_up/n_sig1_nom)-1.0)*100.0 , 2))
          except Exception:
            var_sig1_up = str(0.0)
          try:
            var_sig1_do = str(round(((n_sig1_do/n_sig1_nom)-1.0)*100.0 , 2))
          except Exception:
            var_sig1_do = str(0.0)
          try:
            var_sig2_up = str(round(((n_sig2_up/n_sig2_nom)-1.0)*100.0 , 2))
          except Exception:
            var_sig2_up = str(0.0)
          try:
            var_sig2_do = str(round(((n_sig2_do/n_sig2_nom)-1.0)*100.0 , 2))
          except Exception:
            var_sig2_do = str(0.0)

        print("Background nominal: ",n_bkgn_nom," up: ",n_bkgn_up,"("+var_bkgn_up+"%) down: ",n_bkgn_do,"("+var_bkgn_do+"%)")
        if nLeptons == '0lep':
          print("Signal "+basename_sig1+"nominal: ",n_sig1_nom," up: ",n_sig1_up,"("+var_sig1_up+"%) down: ",n_sig1_do,"("+var_sig1_do+"%)")
          print("Signal "+basename_sig2+"nominal: ",n_sig2_nom," up: ",n_sig2_up,"("+var_sig2_up+"%) down: ",n_sig2_do,"("+var_sig2_do+"%)")


        # up/down variation plots for total background
        c0 = TCanvas("c0+"+region+"_"+variationtype+"_bkgns","c0+"+region+"_"+variationtype+"_bkgns",400,400)
        c0.Draw()
        c0.Divide(1,2)
        top = c0.cd(1)
        top.SetLogy(0)
        top.SetPad(0.0,0.3,1.0,1.0)
        top.SetBottomMargin(0)
        bot = c0.cd(2)
        bot.SetPad(0.0,0.0,1.0,0.3)
        bot.SetFillColor(0)
        bot.SetTopMargin(0)
        bot.SetBottomMargin(0.3)

        top.cd()
        top.SetLogy()
        maxbkgn = h_bkgn.GetBinContent(h_bkgn.GetMaximumBin())
        h_bkgn.GetXaxis().SetRangeUser(xmin,xmax)
        h_bkgn.SetMinimum(0.0001*maxbkgn)
        h_bkgn.SetMaximum(100.0*maxbkgn)
        h_bkgn.GetXaxis().SetTitle(xlabel)
        h_bkgn.GetYaxis().SetTitle("Events / Bin")
        h_bkgn.Draw("hist")
        h_bkgn_up.Draw("histsame")
        if nvar==2:
          h_bkgn_do.Draw("histsame")

        ATLASLabel(0.2,0.90,1,0.13, 0.04,"Internal")
        myText(0.3,0.96,1, 0.04,nLeptons+"_"+str(region))
        myText(0.2,0.85,1, 0.04,"Variation : "+syst_up.split("__")[0])

        bkgLegend = ""
        if variationtype == "SysZPtV" or variationtype == "SysZMbb" or variationtype == "SysMbbZShapeOnly":
          bkgLegend = "Z + jets background"
        elif variationtype == "SysWPtV" or variationtype == "SysWMbb" or variationtype == "SysMbbWShapeOnly":
          bkgLegend = "W + jets background"
        elif variationtype == "SysTTbarPTV" or variationtype == "SysTTbarMBB" or variationtype == "SysNNLORW" or variationtype == "SysMbbTTbarShapeOnly":
          bkgLegend = "t#bar{t} background"
        elif variationtype == "SysStoptPTV" or variationtype == "SysStoptMBB" or variationtype == "SysStopWtPTV" or variationtype == "SysStopWtMBB" or variationtype == "SysMbbStoptShapeOnly" or variationtype == "SysMbbStopWtShapeOnly":
          bkgLegend = "Single top background"
        elif variationtype == "SysVVPTVME" or variationtype == "SysVVMbbME" or variationtype == "SysVVPTVPSUE" or variationtype == "SysVVMbbPSUE" or variationtype == "SysMbbVVMEShapeOnly" or variationtype == "SysMbbVVPSUEShapeOnly":
          bkgLegend = "Diboson background"
        elif variationtype == "SysMJ_Shape0mergedMassPass":
          bkgLegend = "Multijet background"
        elif variationtype == "SysMJ_Shape1mergedMassPass":
          bkgLegend = "Multijet background"
        elif variationtype == "SysMJ_Shape2mergedMassPass":
          bkgLegend = "Multijet background"
        elif variationtype == "SysMJ_Shape0mergedMassFail":
          bkgLegend = "Multijet background"
        elif variationtype == "SysMJ_Shape1mergedMassFail":
          bkgLegend = "Multijet background"
        elif variationtype == "SysMJ_Shape2mergedMassFail":
          bkgLegend = "Multijet background"
        elif variationtype == "SysMJ_Shape0resolvedMassPass":
          bkgLegend = "Multijet background"
        elif variationtype == "SysMJ_Shape1resolvedMassPass":
          bkgLegend = "Multijet background"
        elif variationtype == "SysMJ_Shape2resolvedMassPass":
          bkgLegend = "Multijet background"
        elif variationtype == "SysMJ_Shape0resolvedMassFail":
          bkgLegend = "Multijet background"
        elif variationtype == "SysMJ_Shape1resolvedMassFail":
          bkgLegend = "Multijet background"
        elif variationtype == "SysMJ_Shape2resolvedMassFail":
          bkgLegend = "Multijet background"
        else:
          bkgLegend = "Total Background"
        myLineText(0.3,0.80,0.07,1,1,0.04,str(bkgLegend))
        if nvar==2:
          myLineText(0.3,0.75,0.07,4,1,0.04,"Variation Up (integral variation = "+var_bkgn_up+"%)")
          myLineText(0.3,0.70,0.07,2,1,0.04,"Variation Do (integral variation = "+var_bkgn_do+"%)")
        else:
          myLineText(0.3,0.75,0.07,4,1,0.04,"Variation (integral variation = "+var_bkgn_up+"%)")
          var_bkgn_do = 0.0

        bot.cd()
        bot.SetGrid()
        h_bkgn_ratio_up.SetMinimum(0.5)
        h_bkgn_ratio_up.SetMaximum(1.5)
        h_bkgn_ratio_up.GetXaxis().SetRangeUser(xmin, xmax)
        h_bkgn_ratio_up.GetYaxis().SetTitle("Var./Nom.")
        h_bkgn_ratio_up.GetYaxis().CenterTitle()
        h_bkgn_ratio_up.GetYaxis().SetTitleOffset(0.5)
        h_bkgn_ratio_up.GetYaxis().SetTitleSize(0.13)
        h_bkgn_ratio_up.GetYaxis().SetLabelSize(0.13)
        h_bkgn_ratio_up.GetXaxis().SetTitle(xlabel)
        h_bkgn_ratio_up.GetXaxis().SetTitleOffset(1.0)
        h_bkgn_ratio_up.GetXaxis().SetTitleSize(0.13)
        h_bkgn_ratio_up.GetXaxis().SetLabelSize(0.13)
        h_bkgn_ratio_up.GetYaxis().SetNdivisions(508)
        h_bkgn_ratio_up.Draw("hist")
        if nvar==2:
          h_bkgn_ratio_do.Draw("histsame")
        c0.SaveAs(outputdir+"/SystematicShift_"+region+"_"+variationtype+"_bkgn.pdf")
        c0.SaveAs(outputdir+"/SystematicShift_"+region+"_"+variationtype+"_bkgn.png")


        # up/down variation plots for signal 1
        if nLeptons == '0lep':
          c1 = TCanvas("c1"+region+"_"+variationtype+"_sig1","c1"+region+"_"+variationtype+"_sig1",400,400)
          c1.Draw()
          c1.Divide(1,2)
          top = c1.cd(1)
          top.SetLogy(0)
          top.SetPad(0.0,0.3,1.0,1.0)
          top.SetBottomMargin(0)
          bot = c1.cd(2)
          bot.SetPad(0.0,0.0,1.0,0.3)
          bot.SetFillColor(0)
          bot.SetTopMargin(0)
          bot.SetBottomMargin(0.3)

          top.cd()
          top.SetLogy()
          maxsig1 = h_sig1.GetBinContent(h_sig1.GetMaximumBin())
          h_sig1.GetXaxis().SetRangeUser(xmin,xmax)
          h_sig1.SetMinimum(0.0001*maxsig1)
          h_sig1.SetMaximum(100.0*maxsig1)
          h_sig1.GetXaxis().SetTitle(xlabel)
          h_sig1.GetYaxis().SetTitle("Events / Bin")
          h_sig1.Draw("hist")
          h_sig1_up.Draw("histsame")
          if nvar==2:
            h_sig1_do.Draw("histsame")
          ATLASLabel(0.2,0.90,1,0.13, 0.04,"Internal")
          myText(0.3,0.96,1, 0.04,nLeptons+"_"+str(region))
          myText(0.2,0.85,1, 0.04,"Variation : "+syst_up.split("__")[0])
          myLineText(0.3,0.80,0.07,1,1,0.04,"Signal: "+str(basename_sig1))
          if nvar==2:
            myLineText(0.3,0.75,0.07,4,1,0.04,"Variation Up (integral variation = "+var_sig1_up+"%)")
            myLineText(0.3,0.70,0.07,2,1,0.04,"Variation Do (integral variation = "+var_sig1_do+"%)")
          else:
            myLineText(0.3,0.75,0.07,4,1,0.04,"Variation (integral variation = "+var_sig1_up+"%)")
            var_sig1_do = 0.0

          bot.cd()
          bot.SetGrid()
          h_sig1_ratio_up.SetMinimum(0.5)
          h_sig1_ratio_up.SetMaximum(1.5)
          h_sig1_ratio_up.GetXaxis().SetRangeUser(xmin,xmax)
          h_sig1_ratio_up.GetYaxis().SetTitle("Var./Nom.")
          h_sig1_ratio_up.GetYaxis().CenterTitle()
          h_sig1_ratio_up.GetYaxis().SetTitleOffset(0.5)
          h_sig1_ratio_up.GetYaxis().SetTitleSize(0.13)
          h_sig1_ratio_up.GetYaxis().SetLabelSize(0.13)
          h_sig1_ratio_up.GetXaxis().SetTitle(xlabel)
          h_sig1_ratio_up.GetXaxis().SetTitleOffset(1.0)
          h_sig1_ratio_up.GetXaxis().SetTitleSize(0.13)
          h_sig1_ratio_up.GetXaxis().SetLabelSize(0.13)
          h_sig1_ratio_up.GetYaxis().SetNdivisions(508)
          h_sig1_ratio_up.Draw("hist")
          if nvar==2:
            h_sig1_ratio_do.Draw("histsame")

          if options.lepton == '0lep':
            c1.SaveAs(outputdir+"/SystematicShift_"+region+"_"+variationtype+"_sig1.pdf")
            c1.SaveAs(outputdir+"/SystematicShift_"+region+"_"+variationtype+"_sig1.png")

        # up/down variation plots for signal 2
        if nLeptons == '0lep':
          c2 = TCanvas("c2"+region+"_"+variationtype+"_sig2","c2"+region+"_"+variationtype+"_sig2",400,400)
          c2.Draw()
          c2.Divide(1,2)
          top = c2.cd(1)
          top.SetLogy(0)
          top.SetPad(0.0,0.3,1.0,1.0)
          top.SetBottomMargin(0)
          bot = c2.cd(2)
          bot.SetPad(0.0,0.0,1.0,0.3)
          bot.SetFillColor(0)
          bot.SetTopMargin(0)
          bot.SetBottomMargin(0.3)

          top.cd()
          top.SetLogy()
          maxsig2 = h_sig2.GetBinContent(h_sig2.GetMaximumBin())
          h_sig2.GetXaxis().SetRangeUser(xmin,xmax)
          h_sig2.SetMinimum(0.0001*maxsig2)
          h_sig2.SetMaximum(100.0*maxsig2)
          h_sig2.GetXaxis().SetTitle(xlabel)
          h_sig2.GetYaxis().SetTitle("Events / Bin")
          h_sig2.Draw("hist")
          h_sig2_up.Draw("histsame")
          if nvar==2:
            h_sig2_do.Draw("histsame")
          ATLASLabel(0.2,0.90,1,0.13, 0.04,"Internal")
          myText(0.3,0.96,1, 0.04,nLeptons+"_"+str(region))
          myText(0.2,0.85,1, 0.04,"Variation : "+syst_up.split("__")[0])
          myLineText(0.3,0.80,0.07,1,1,0.04,"Signal: "+str(basename_sig2))
          if nvar==2:
            myLineText(0.3,0.75,0.07,4,1,0.04,"Variation Up (integral variation = "+var_sig2_up+"%)")
            myLineText(0.3,0.70,0.07,2,1,0.04,"Variation Do (integral variation = "+var_sig2_do+"%)")
          else:
            myLineText(0.3,0.75,0.07,4,1,0.04,"Variation (integral variation = "+var_sig2_up+"%)")
            var_sig2_do = 0.0

          bot.cd()
          bot.SetGrid()
          h_sig2_ratio_up.SetMinimum(0.5)
          h_sig2_ratio_up.SetMaximum(1.5)
          h_sig2_ratio_up.GetXaxis().SetRangeUser(xmin,xmax)
          h_sig2_ratio_up.GetYaxis().SetTitle("Var./Nom.")
          h_sig2_ratio_up.GetYaxis().CenterTitle()
          h_sig2_ratio_up.GetYaxis().SetTitleOffset(0.5)
          h_sig2_ratio_up.GetYaxis().SetTitleSize(0.13)
          h_sig2_ratio_up.GetYaxis().SetLabelSize(0.13)
          h_sig2_ratio_up.GetXaxis().SetTitle(xlabel)
          h_sig2_ratio_up.GetXaxis().SetTitleOffset(1.0)
          h_sig2_ratio_up.GetXaxis().SetTitleSize(0.13)
          h_sig2_ratio_up.GetXaxis().SetLabelSize(0.13)
          h_sig2_ratio_up.GetYaxis().SetNdivisions(508)
          h_sig2_ratio_up.Draw("hist")
          if nvar==2:
            h_sig2_ratio_do.Draw("histsame")

          if options.lepton == '0lep':
            c2.SaveAs(outputdir+"/SystematicShift_"+region+"_"+variationtype+"_sig2.pdf")
            c2.SaveAs(outputdir+"/SystematicShift_"+region+"_"+variationtype+"_sig2.png")


        # write table
        TempSystSizes[variationtype] = {}
        TempSystSizes[variationtype]["var_bkgn_up"] = var_bkgn_up
        TempSystSizes[variationtype]["var_bkgn_do"] = var_bkgn_do
        if nLeptons == '0lep':
          TempSystSizes[variationtype]["var_sig1_up"] = var_sig1_up
          TempSystSizes[variationtype]["var_sig1_do"] = var_sig1_do
          TempSystSizes[variationtype]["var_sig2_up"] = var_sig2_up
          TempSystSizes[variationtype]["var_sig2_do"] = var_sig2_do

        SystSizes[region] = TempSystSizes
        tableName = outputdir+"/tables_"+nLeptons+"_"+region_sr+".tex"
        with open(tableName, 'w') as file:
          for region in SystSizes.keys():
            print("\n\n\nSUMMARY FOR: ",region)
            line1 = "Systematic Source & Background Variation & %s & %s \\\\ \\midrule \n"%(basename_sig1,basename_sig2)
            line2 = "& (up,down)  & (up,down) & (up,down)   \\\\ \\midrule \n"
            print(line1)
            print(line2)
            file.write(line1)
            file.write(line2)
            for syst in sorted(SystSizes[region].keys()):
              var_bkgn_up = SystSizes[region][syst]["var_bkgn_up"]
              var_bkgn_do = SystSizes[region][syst]["var_bkgn_do"]
              if nLeptons == '0lep':
                var_sig1_up = SystSizes[region][syst]["var_sig1_up"]
                var_sig1_do = SystSizes[region][syst]["var_sig1_do"]
                var_sig2_up = SystSizes[region][syst]["var_sig2_up"]
                var_sig2_do = SystSizes[region][syst]["var_sig2_do"]

              if nLeptons == "1lep" or nLeptons == "2lep":
                var_sig1_up = 0.00
                var_sig1_do = 0.00
                var_sig2_up = 0.00
                var_sig2_do = 0.00

              if pruning == "True":
                treshold = 0.02
                if math.fabs(float(var_bkgn_up)) < treshold and math.fabs(float(var_bkgn_do)) < treshold and math.fabs(float(var_sig1_up)) < treshold and math.fabs(float(var_sig1_do)) < treshold and math.fabs(float(var_sig2_up)) < treshold and math.fabs(float(var_sig2_do)) < treshold:
                  continue

              printsyst = syst.replace("_","\\_")
              print("[[ \% ]] \%")
              if math.fabs(float(var_bkgn_up))>1.0 or math.fabs(float(var_bkgn_do))>1.0 or math.fabs(float(var_sig1_up))>1.0 or math.fabs(float(var_sig1_do))>1.0 or math.fabs(float(var_sig2_up))>1.0 or math.fabs(float(var_sig2_do))>1.0:
                printsyst = "\\textbf{"+printsyst+"}"

              if syst=="SysJET_JER_SINGLE_NP" or syst=="SysMET_SoftTrk_ResoPerp" or syst=="SysMET_SoftTrk_ResoPara" or syst=="SysFATJET_JER" or syst=="SysFATJET_JMR" or syst=="SysFATJET_JD2R":
                line = "{0:40}  &  {1:20}       &  {2:20}       &  {3:20}      \\\\ \\midrule\n".format(printsyst,var_bkgn_up,var_sig1_up,var_sig2_up)
                print(line)
                file.write(line)
              else:
                line = "{0:40}  &  ({1:10} , {2:10})  &  ({3:10} , {4:10})  &  ({5:10} , {6:10}) \\\\ \\midrule\n".format(printsyst,var_bkgn_up,var_bkgn_do,var_sig1_up,var_sig1_do,var_sig2_up,var_sig2_do)
                print(line)
                file.write(line)

  csvFile.close()

if __name__=='__main__':
  main()
