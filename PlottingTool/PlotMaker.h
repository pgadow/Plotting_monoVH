#ifndef PLOTMAKER_H
#define PLOTMAKER_H

#include "Config.h"
#include "SystTool.h"
#include "LatexMaker.h"

#include <TFile.h>
#include <TH2.h>
#include <THStack.h>

class PlotMaker {

public:
  PlotMaker();
  ~PlotMaker();

  void makePlots(Config &config);

  struct Sensitivity {
    std::string var;
    std::string region;
    std::string trafo;
    double *sigmaStat;
    double *sigmaNorm;
    double *sigmaSyst;
    double *DLLR;
  };

  struct Histos {
    
    std::string varName;
    THStack *hs_bkg;
    THStack *hs_bkgOnly;
    TH1 *h_bkg;
    TH1 *h_bkgWithSys;
    TH1 *h_bkgSys;
    TH1 *h_bkgWithShapeSys;
    TH1 *h_bkgShapeSys;
    TH1 *h_bkgStat;
    TH1 *h_data;
    TH1 *h_dataUnblinded;
    TH1 *h_signal;
    TH1 *h_signalUnscaled;
    std::vector<TH1*> h_signals;
    std::vector<SystTool::SystHolder> systHolders;
    Sensitivity sensitivity;
    bool isValid;
    bool isBlindedX;
    bool isBlindedY;
    TH1 *h_bkg_profileX_stat;
    TH1 *h_bkg_profileX_RMS;
    TH1 *h_bkg_profileY_stat;
    TH1 *h_bkg_profileY_RMS;
    TH1 *h_signal_profileX_stat;
    TH1 *h_signal_profileX_RMS;
    TH1 *h_signal_profileY_stat;
    TH1 *h_signal_profileY_RMS;
    TH1 *h_data_profileX_stat;
    TH1 *h_data_profileX_RMS;
    TH1 *h_data_profileY_stat;
    TH1 *h_data_profileY_RMS;
  };


private:

  Sensitivity makePlot(Config &config, Config::Variable var, Config::Region region, Config::BDTTransform trafo, bool &doTables);
  void make2DPlot(Config &config, Config::Variable var1, Config::Variable var2, Config::Region region);
  void makeSysTable(Config &config, Histos h, Config::Region region);
  void makeYieldTable(Config &config, Histos h, Config::Region region, bool yieldForEveryPlot);
  void makeSystPlots(Config &config, Config::SystematicPlot &sysPlot, Histos h, 
		     Config::Variable var, Config::Region region, Config::BDTTransform trafo);

  Histos readHistos(Config &config, Config::Variable var, Config::Region region, Config::BDTTransform trafo);
  Histos readHistos2D(Config &config, Config::Variable &var1, Config::Variable &var2, Config::Region region);
  std::vector<SystTool::SystHolder> readSystHolders(Config &config, 
						    TH1 *h_nominal, 
						    Config::Variable var,
						    Config::Region region,
						    Config::BDTTransform trafo);

  void applyBlinding(Config &config, Histos &h, std::string varName, Config::Region region, Config::BDTTransform trafo);
  void applyThresholdBlinding(Histos  &h, double threshold);
  void applyWindowBlinding(Histos  &h, double xmin, double xmax);
  void applyBlinding2D(Config &config, Histos  &h, std::string varName1, std::string varName2, Config::Region region);
  void applyThresholdBlinding2D(Histos  &h, double threshold);
  void applyWindowBlinding2D(Histos  &h, double xmin, double xmax, int iAxis);

  void drawErrorBand(TH1 *h);

  SystTool *m_systTool;
  TFile *m_file;
  LatexMaker *m_latexMaker;

  

};

#endif
