#include "Utils.h"

#include "transform/HistoTransform.h"

#include <iostream>
#include <fstream>

std::vector<std::string> Utils::tokenize(std::string str, std::string delim){

  std::vector<std::string> tokens;
  std::string::size_type sPos, sEnd, sLen;
  sPos = str.find_first_not_of(delim);
  while ( sPos != std::string::npos ) {
    sEnd = str.find_first_of(delim, sPos);
    if(sEnd==std::string::npos) sEnd = str.length();
    sLen = sEnd - sPos;
    std::string word = str.substr(sPos,sLen);
    tokens.push_back(word);
    sPos = str.find_first_not_of(delim, sEnd);
  }
  return tokens;
}


void Utils::replaceAll(std::string& str, const std::string& from, const std::string& to) {
 
  size_t start_pos = 0;
  while((start_pos = str.find(from, start_pos)) != std::string::npos) {
    size_t end_pos = from.length();
    str.replace(start_pos, end_pos, to);
    start_pos += to.length();
  }
}

TObject *Utils::readObj(TFile *f, const char *name, bool allowMissing) {

  TObject *obj = f->Get(name);

  if(obj == 0) {
    if(!allowMissing) {
      std::cout << "Object with name " << name << " was not found in file " << f->GetName() << std::endl;
    }
    return 0;
  }

  return obj->Clone();
}

TH1 *Utils::readHist(TFile *f, const char *name, bool allowMissing, double xscale, double yscale) {

  TObject *obj = readObj(f, name, allowMissing);
  if(obj) {
    if (xscale != 1.0 || yscale != 1.0)
      Utils::scaleAxes((TH1*)obj, xscale, yscale);
    return (TH1*)obj;
  }

  return 0;
}

TH1 *Utils::getRelErrors(TH1 *h) {

  TH1 *h_err = (TH1*)h->Clone();
  h_err->SetDirectory(0);
  h_err->Reset();

  for(int bin=0; bin<=h->GetNbinsX()+1; bin++) {
    float content = h->GetBinContent(bin);
    float error = h->GetBinError(bin);
    if(content != 0) {
      h_err->SetBinContent(bin, 0);
      h_err->SetBinError(bin, error / content);
    }
  }

  return h_err;
}

TH1 *Utils::getUnblindedBins(TH1 *h1, TH1 *h2) {

  TH1 *h = (TH1*) h2->Clone();

  for(int bin=1; bin<=h1->GetNbinsX(); bin++) {

    double y1 = h1->GetBinContent(bin);

    double e1 = h1->GetBinError(bin);

    if(y1 == 0 && e1 == 0) {
      h->SetBinContent(bin, 0);
      h->SetBinError(bin, 0);
    }
  }

  return h;
}

double Utils::getChi2(TH1 *h1, TH1 *h2, bool shapeOnly) {

  h2 = getUnblindedBins(h1, h2);

  double chi2 = 0;
  double ndf = 0;

  double scale = 1;
  if(shapeOnly) {
    scale = h1->Integral() / h2->Integral();
  }

  for(int bin=1; bin<=h1->GetNbinsX(); bin++) {

    double y1 = h1->GetBinContent(bin);
    double y2 = h2->GetBinContent(bin) * scale;

    double e1 = h1->GetBinError(bin);
    double e2 = h2->GetBinError(bin) * scale;

    if(y1 > 0 && y2 > 0 && e1 > 0 && e2 > 0) {

      double d = y2 - y1;
      double e = e1*e1 + e2*e2;

      chi2 += d * d / e;
      ndf += 1;
    }
  }

  chi2 = chi2 / (ndf - 1);

  delete h2;

  return chi2;
}

double Utils::getKSProb(TH1 *h1, TH1 *h2) {

  h2 = getUnblindedBins(h1, h2);

  double ksProb = h1->KolmogorovTest(h2);

  delete h2;

  return ksProb;
}

TH1 *Utils::getDataMCComparison(TH1 *h_data, TH1 *h_MC) {

  TH1 *h_comp = (TH1*)h_data->Clone();
  h_comp->Reset();

  for(int bin=1; bin<=h_comp->GetNbinsX(); bin++) {

    double nData = h_data->GetBinContent(bin);
    double eData = h_data->GetBinError(bin);
    double nMC = h_MC->GetBinContent(bin);

    if(nData > 1e-6 && eData > 1e-6 && nMC > 1e-6) {

      double nComp = (nData - nMC) / nMC;
      double eComp = eData / nMC;

      h_comp->SetBinContent(bin, nComp);
      h_comp->SetBinError(bin, eComp);
    }
  }

  return h_comp;
}

TH1 *Utils::getSymmetricVariation(TH1 *h_nominal, TH1 *h_var) {

  TH1 *h_varOpp = (TH1*)h_var->Clone();
  h_varOpp->Reset();

  for(int bin=1; bin<=h_varOpp->GetNbinsX(); bin++) {

    double nominal = h_nominal->GetBinContent(bin);
    double var = h_var->GetBinContent(bin);

    double varOpp = 2 * nominal - var;

    h_varOpp->SetBinContent(bin, varOpp);
  }

  return h_varOpp;
}

vector<int> Utils::getRebinBins(TH1* histoBkg, TH1* histoSig, int method, float trafoPar1, float trafoPar2) {

  vector<int> bins;
  bins.clear();

  float maxUnc = trafoPar1;

  HistoTransform histoTrafo;
  histoTrafo.trafoFiveY = trafoPar2;
  histoTrafo.trafoSixY = trafoPar1;
  histoTrafo.trafoSixZ = trafoPar2;
  histoTrafo.trafoSevenNBins = trafoPar1;
  histoTrafo.trafoEightRebin = trafoPar1;
  histoTrafo.trafoEzSig = trafoPar1;
  histoTrafo.trafoEzBkg = trafoPar2;
  histoTrafo.trafoFzSig = trafoPar1;
  histoTrafo.trafoFzBkg = trafoPar2;

  bins = histoTrafo.getRebinBins(histoBkg, histoSig, method, maxUnc);

  return bins;
}

void Utils::rebinHisto(TH1* histo, vector<int> &bins, bool equidist, bool relabel) {

  if(!histo) return;

  HistoTransform histoTrafo;
  histoTrafo.rebinHisto(histo, &bins, equidist, relabel);
}

double *Utils::getExpectedSignificance(TH1 *hBkg, TH1 *hSig, SensitivityMethod method, TH1 *h_bkgSys, bool normSys) {
  
  double *sig = 0;
  
  if(!hBkg || !hSig) {
     return sig;
  }
  
  if(hBkg->GetNbinsX() != hSig->GetNbinsX()) {
    return sig;
  }
  
  if(h_bkgSys && h_bkgSys->GetNbinsX() != hBkg->GetNbinsX()) {
    return sig;
  }

  sig = new double[2];
  sig[0] = 0;
  sig[1] = 0;

  int nbins = hSig->GetNbinsX();
  
  double scaleSys = 0;
  if(normSys && h_bkgSys) {
    double I = hBkg->Integral();
    if(I > 0) {
      for(int bin=1; bin<=h_bkgSys->GetNbinsX(); bin++) {
	scaleSys += h_bkgSys->GetBinError(bin);
      }
      scaleSys /= I;
    }
  }

  for(int bin=1; bin<=nbins; bin++) {
    
    double iS = hSig->GetBinContent(bin);
    double iB = hBkg->GetBinContent(bin);
    double idS = hSig->GetBinError(bin);
    double idB = hBkg->GetBinError(bin);
    
    double dB = 0;
    if(normSys) {
      dB = scaleSys * iB;
    }else{
      if(h_bkgSys) {
	dB = h_bkgSys->GetBinError(bin);
      }
    }

    if( iB > 0 && iS > 0) {

      switch(method) {
      case Normal:
	sig[0] += (iS*iS) / (iB + dB*dB); 
	sig[1] += ( (iS*iS) / (iB*iB) ) * ( idS * idS + ( (iS*iS) / (4*iB*iB) ) * idB * idB ); 
	break;
      case DeltaLLR:
	{
	  double iLSB = log(1 + iS/iB);
	  sig[0] += 2 * ( (iS+iB) * iLSB - iS );
	  sig[1] += iLSB * iLSB * idS * idS + (iLSB - iS/iB) * (iLSB - iS/iB) * idB * idB;
	}
	break;
      default:
	break;
      }
    }
  }

  sig[0] = sqrt(sig[0]);
  if(sig[0] > 0) {
    sig[1] = sqrt(sig[1]) / sig[0];
  }
  return sig;
}

void Utils::getProfileX(TH2 *h, TH1 **h_RMS, TH1 **h_stat) {

  (*h_RMS) = (TH1*)h->ProjectionX()->Clone("rms");
  (*h_RMS)->SetDirectory(0);
  (*h_RMS)->Reset();
  (*h_stat) = (TH1*)h->ProjectionX()->Clone("stat");
  (*h_stat)->SetDirectory(0);
  (*h_stat)->Reset();

  for(int binx=1; binx<=h->GetNbinsX(); binx++) {

    TH1 *h_slice = h->ProjectionY("py", binx, binx);
    double mean = h_slice->GetMean();
    double rms = h_slice->GetRMS();
    double err = h_slice->GetMeanError();
    delete h_slice;

    (*h_RMS)->SetBinContent(binx, mean);
    (*h_RMS)->SetBinError(binx, rms);
    (*h_stat)->SetBinContent(binx, mean);
    (*h_stat)->SetBinError(binx, err);
  }

}

void Utils::getProfileY(TH2 *h, TH1 **h_RMS, TH1 **h_stat) {

  (*h_RMS) = (TH1*)h->ProjectionY()->Clone("rms");
  (*h_RMS)->SetDirectory(0);
  (*h_RMS)->Reset();
  (*h_stat) = (TH1*)h->ProjectionY()->Clone("stat");
  (*h_stat)->SetDirectory(0);
  (*h_stat)->Reset();

  for(int biny=1; biny<=h->GetNbinsY(); biny++) {

    TH1 *h_slice = h->ProjectionX("py", biny, biny);
    double mean = h_slice->GetMean();
    double rms = h_slice->GetRMS();
    double err = h_slice->GetMeanError();
    delete h_slice;

    (*h_RMS)->SetBinContent(biny, mean);
    (*h_RMS)->SetBinError(biny, rms);
    (*h_stat)->SetBinContent(biny, mean);
    (*h_stat)->SetBinError(biny, err);
  }

}

void Utils::adjustMax(TH1 *h_bkg, TH1 *h_data, bool useData, double scaleMax) {

  double max = h_bkg->GetBinContent(1)+h_bkg->GetBinError(1);
  double min = max;
  for(int bin=2; bin<=h_bkg->GetNbinsX(); bin++) {
    double y = h_bkg->GetBinContent(bin)+h_bkg->GetBinError(bin);
    if(y > max) max = y;
    if(y < min) min = y;
  }

  if(useData) {
    for(int bin=1; bin<=h_data->GetNbinsX(); bin++) {
      double y = h_data->GetBinContent(bin)+h_data->GetBinError(bin);
      if(y > max) max = y;
      if(y < min) min = y;
    }
  }

  h_bkg->SetMaximum(min + (max - min)*scaleMax);
}

bool Utils::doesFileExist(std::string fname) {

  std::ifstream ifile(fname.c_str());
  return ifile;
}

void Utils::scaleAxes(TH1 *h, double xscale, double yscale) {

  TH1 *h_bkp = (TH1*)h->Clone();

  int nbinsx;
  double *binsx = Utils::getBins(h_bkp->GetXaxis(), xscale, nbinsx);
  if(h->InheritsFrom("TH2")) {
    int nbinsy;
    double *binsy = Utils::getBins(h_bkp->GetYaxis(), yscale, nbinsy);
    h->SetBins(nbinsx, binsx, nbinsy, binsy);
    //    h->Reset();
    h->Reset("ICE"); // don't reset GetEntries. The total x weighted sums fTsumwx and fTsumwx2 will be wrong but can be recalculated and set using PutStats
    for(int binx=0; binx<=nbinsx+1; binx++) {
      for(int biny=0; biny<=nbinsy+1; biny++) {
	h->SetBinContent(binx, biny, h_bkp->GetBinContent(binx, biny));
	h->SetBinError(binx, biny, h_bkp->GetBinError(binx, biny));
      }
    }
    delete [] binsy;
  }else{
    h->SetBins(nbinsx, binsx);
    //    h->Reset();
    h->Reset("ICE"); // don't reset GetEntries. The total x weighted sums fTsumwx and fTsumwx2 will be wrong but can be recalculated and set using PutStats
    for(int binx=0; binx<=nbinsx+1; binx++) {
      h->SetBinContent(binx, h_bkp->GetBinContent(binx));
      h->SetBinError(binx, h_bkp->GetBinError(binx));
    }
  }
  delete [] binsx;
  delete h_bkp;
}

double *Utils::getBins(TAxis *axis, double scale, int &nbins) {

  nbins = axis->GetNbins();
  double *bins = new double[nbins+1];

  for(int i=0; i<nbins; i++) {
    bins[i] = axis->GetBinLowEdge(i+1) * scale;
  } 
  bins[nbins] = axis->GetBinUpEdge(nbins) * scale;

  return bins;
}

