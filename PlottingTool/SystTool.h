// Author: Georges Aad

#ifndef SystTool_H
#define SystTool_H

#include <iostream>
#include <vector>

#include <TH1.h>
#include <TRandom3.h>
#include <TMath.h>

class SystTool{

public:

  SystTool();
  ~SystTool();

  class SystHolder{

  public:

    SystHolder();
    ~SystHolder();

    std::string name;

    TH1 *m_nominal;
    TH1* m_systUp;
    TH1* m_systDo;
    
    double m_shift;
    double m_constraint;

    bool isValid();

  };

  struct RawData {
    double mean;
    double RMS;
    double shapeMean;
    double shapeRMS;
  };

  std::vector<RawData> getSymmetricShapeSystVector(TH1* h_default, 
						   const std::vector<SystHolder>& systs, 
						   bool resetSeed=true) const;

  //// the client owns the pointer
  TH1** getSymmetricShapeUncertainties(TH1* h_default, 
				       const std::vector<SystHolder>& systs, 
				       bool changeMean=false,
				       bool resetSeed=true) const;

  inline void setRandomNumber(unsigned int rand){m_rand->SetSeed(rand); m_seed=rand;}
  inline void setNExperiments(unsigned int nexp){m_nExp=nexp;}

private:

  std::vector<std::pair<double, double> > getExpData(TH1* h_default, const std::vector<SystHolder>& systs) const;
  std::vector<RawData> computeMeanAndRMS(std::vector<double> bindataMean, 
					 std::vector<double> bindataRMS,
					 std::vector<double> bindataShapeMean, 
					 std::vector<double> bindataShapeRMS) const;


  TRandom* m_rand;
  unsigned int m_nExp;
  unsigned int m_seed;

};

int compareSystHolders(SystTool::SystHolder syst1, SystTool::SystHolder syst2);
std::vector<SystTool::SystHolder> sortSystHolders(std::vector<SystTool::SystHolder> systHolders);

#endif 
