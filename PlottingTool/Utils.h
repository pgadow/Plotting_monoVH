#ifndef UTILS_H
#define UTILS_H

#include <string>

#include <TObject.h>
#include <TH2.h>
#include <TFile.h>
#include <TAxis.h>

namespace Utils {

  std::vector<std::string> tokenize(std::string str, std::string delim);
  void replaceAll(std::string& str, const std::string& from, const std::string& to);
 

  TObject *readObj(TFile *f, const char *name, bool allowMissing = false);
  TH1 *readHist(TFile *f, const char *name, bool allowMissing = false, double xscale=1., double yscale=1.);

  TH1 *getRelErrors(TH1 *h);
  
  TH1 *getUnblindedBins(TH1 *h1, TH1 *h2);

  double getChi2(TH1 *h1, TH1 *h2, bool shapeOnly);
  double getKSProb(TH1 *h1, TH1 *h2);

  TH1 *getDataMCComparison(TH1 *h_data, TH1 *h_MC);

  TH1 *getSymmetricVariation(TH1 *h_nominal, TH1 *h_var);

  vector<int> getRebinBins(TH1* histoBkg, TH1* histoSig, int method, float trafoPar1, float trafoPar2=0);
  void rebinHisto(TH1* histo, vector<int> &bins, bool equidist=true, bool relabel=false);

  enum SensitivityMethod {
    Normal,
    DeltaLLR
  };
  double *getExpectedSignificance(TH1 *h_B, TH1 *h_S, SensitivityMethod method=Normal, TH1 *h_bkgSys=0, bool normSys=false);

  void getProfileX(TH2 *h, TH1 **h_RMS, TH1 **h_stat);
  void getProfileY(TH2 *h, TH1 **h_RMS, TH1 **h_stat);

  void adjustMax(TH1 *h_bkg, TH1 *h_data, bool useData, double scaleMax);

  bool doesFileExist(std::string fname);

  void scaleAxes(TH1 *h, double xscale, double yscale=1);
  double *getBins(TAxis *axis, double scale, int &nbins);
}

#endif
