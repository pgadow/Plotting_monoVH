#include "TF1.h"
#include "TH1F.h"
#include "HistoTransform.h"

void example() {

  // create example histograms
  TH1* hBkg = new TH1F("hBkg","hBkg", 100, -5., 5.);
  TH1* hSig = new TH1F("hSig","hSig", 100, -5., 5.);
  hBkg -> FillRandom("pol0", 1000);
  hSig -> FillRandom("gaus", 1000);

  // create transformation
  HistoTransform histoTrafo;
  histoTrafo.trafoFzSig = 4.5;
  histoTrafo.trafoFzBkg = 4.5;
  vector<int> bins = histoTrafo.getRebinBins(hBkg, hSig, 12);

  // transform any histogram
  histoTrafo.rebinHisto(hBkg, &bins);
  histoTrafo.rebinHisto(hSig, &bins);

  // draw 
  hSig -> SetLineColor(kRed);
  hSig -> Draw("");
  hBkg -> Draw("same");
}
