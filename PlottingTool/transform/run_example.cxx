
// Example for running the transformation code. Execute with:
// root run_example.cxx

void run_example() {
  //gSystem -> AddIncludePath("-I../TransformTool");
  //gROOT->ProcessLine(".L TransformTool/HistoTransform.C+");
  gROOT->ProcessLine(".L HistoTransform.C+");
  gROOT->ProcessLine(".x example.cxx+");
}
