////////////////////////////////////////////
//makePlots0Lepton_monoVH
//
// This is the script used to make the bare minimum number of plots
// for the monoV analysis.  There are many additional features of this plotting
// package that a good physicist would explore by reading the code ... GET SOME!
//
////////////////////////////////////////////

#include "PlottingTool/Config.h"
#include "PlottingTool/PlotMaker.h"
#include <TROOT.h>
#include <iostream>
#include <string>
#include <vector>
#include <ctime>

using namespace std;

std::string getDate() {
  time_t rawtime;
  struct tm * timeinfo;
  char buffer[80];

  time(&rawtime);
  timeinfo = localtime(&rawtime);

  strftime(buffer, 80, "%Y-%m-%d", timeinfo);
  std::string str(buffer);

  return str;
}

void makePlots0Lepton_monoVH(std::string InputPath = "./", std::string InputFile = "inputsFile") {

  Config config;

  // should we overwrite existing plots or only do missing ones?
  config.setOverwrite(true);

  // path to the input file: can be local or on eos
  config.setInputPath(InputPath);

  // Since your input file name might not follow the conventions above,
  // it can be overwritten using the line below
  // Note: specify the name WITHOUT ".root" at the end
  config.setInputFile(InputFile);

  // If input histograms are stored in a subdirectory of the input file, specify below
  // config.setInputTDirectory("OneLepton");

  ////////////////
  // General info
  ////////////////

  config.setAnalysis( Config::ZeroLepton,	 // Type = ZeroLepton, OneLepton or TwoLepton: Appears on the plots
		      "Internal",		                   // Status: Appear on the plots next to ATLAS label
		      "Moriond2016",		               // Name = "LHCP2013": only useful for the above naming convention
		      "13",			                       // Center of mass energy: Appears on the plots
		      "36.1",		                       // Integrated luminosity: Appears on the plots
		      "125",		                       // Mass of the generated higgs boson: only useful for the above naming convention
		      "MPI",		                       // Name of your institute: only usefule for the above naming convention
		      "fram15");		                   // Input file version: only useful for the above naming convention

  config.setAnalysisTitle("monoV(qq)");

  // include systematics
  bool doSystematics = true;

  // do up/down systematics plots
  bool addSystematicVariationPlots = false;


  /////////////////
  // OUTPUT PATH - relative to current directory
  /////////////////
  // config.setOutputPath("./plots0Lepton");

  std::string directory = "/ptmp/mpp/pgadow/monoV/Plots/"+getDate()+"/"+getDate()+"plots0Lepton/";
  std::string command = ".!mkdir -p " + directory;
  gROOT->ProcessLine(command.c_str());
  config.setOutputPath(directory);
  std::cout << "Output will be stored in " << directory << std::endl;

  /////////////////
  // SCALE FACTORS
  /////////////////
  //std::string scaleFactorsTag = "SF_0L_nJ_1128";
  //std::string scaleFactorsTag = "data15Only";
  //config.readScaleFactors("PlottingTool/scaleFactors/"+scaleFactorsTag+".txt");

  /////////////////////
  // SAMPLES
  /////////////////////

  config.addDataSample("data", "Data", 1);

  config.addBackgroundSample("multijet0mergedMassPass", "Multijet template", kPink+1);
  config.addBackgroundSample("multijet1mergedMassPass", "Multijet template", kPink+1);
  config.addBackgroundSample("multijet2mergedMassPass", "Multijet template", kPink+1);
  config.addBackgroundSample("multijet0resolvedMassPass", "Multijet template", kPink+1);
  config.addBackgroundSample("multijet1resolvedMassPass", "Multijet template", kPink+1);
  config.addBackgroundSample("multijet2resolvedMassPass", "Multijet template", kPink+1);
  config.addBackgroundSample("multijet0mergedMassFail", "Multijet template", kPink+1);
  config.addBackgroundSample("multijet1mergedMassFail", "Multijet template", kPink+1);
  config.addBackgroundSample("multijet2mergedMassFail", "Multijet template", kPink+1);
  config.addBackgroundSample("multijet0resolvedMassFail", "Multijet template", kPink+1);
  config.addBackgroundSample("multijet1resolvedMassFail", "Multijet template", kPink+1);
  config.addBackgroundSample("multijet2resolvedMassFail", "Multijet template", kPink+1);
  config.addBackgroundSample("multijet0merged", "Multijet template", kPink+1);
  config.addBackgroundSample("multijet1merged", "Multijet template", kPink+1);
  config.addBackgroundSample("multijet2merged", "Multijet template", kPink+1);
  config.addBackgroundSample("multijet0resolved", "Multijet template", kPink+1);
  config.addBackgroundSample("multijet1resolved", "Multijet template", kPink+1);
  config.addBackgroundSample("multijet2resolved", "Multijet template", kPink+1);

  config.addBackgroundSample("WW",      "Diboson", kGray+1);
  config.addBackgroundSample("WZ",      "Diboson", kGray+1);
  config.addBackgroundSample("ZZ",      "Diboson", kGray+1);

  //config.addBackgroundSample("W",      "W (inclusive)", kGreen-10);
  config.addBackgroundSample("Wl",     "W+ll", kGreen-9);
  config.addBackgroundSample("Wcl",    "W+cl", kGreen-6);
  config.addBackgroundSample("Wbb",    "W+(bb,bc,bl,cc)", kGreen+3);
  config.addBackgroundSample("Wbc",    "W+(bb,bc,bl,cc)", kGreen+3);
  config.addBackgroundSample("Wbl",    "W+(bb,bc,bl,cc)", kGreen+3);
  config.addBackgroundSample("Wcc",    "W+(bb,bc,bl,cc)", kGreen+3);

  //config.addBackgroundSample("Z",      "Z (inclusive)", kAzure-10);
  config.addBackgroundSample("Zl",     "Z+ll", kAzure-9);
  config.addBackgroundSample("Zcl",    "Z+cl", kAzure-8);
  config.addBackgroundSample("Zbb",    "Z+(bb,bc,bl,cc)", kAzure+2);
  config.addBackgroundSample("Zbc",    "Z+(bb,bc,bl,cc)", kAzure+2);
  config.addBackgroundSample("Zbl",    "Z+(bb,bc,bl,cc)", kAzure+2);
  config.addBackgroundSample("Zcc",    "Z+(bb,bc,bl,cc)", kAzure+2);

  config.addBackgroundSample("stops",  "Single top", kOrange-1);
  config.addBackgroundSample("stopt",  "Single top", kOrange-1);
  config.addBackgroundSample("stopWt", "Single top", kOrange-1);

  config.addBackgroundSample("ttbar",   "t#bar{t}", kOrange);
  // config.addBackgroundSample("dijetJZW", "Dijet", kPink+1);


  //this is only for 0 lepton channel
  config.addSignalSample("dmVWhadDM1MM600","dmVWhadDM1MM600", kBlue);
  // config.addSignalSample("dmVZhadDM1MM300","dmVZhadDM1MM300", kRed);
  config.setNoSignalStack(true);

  //////////////
  // VARIABLES
  //////////////

  config.addVariable(Config::BDTInput, "MET", "E_{T}^{miss} [GeV]", {150,200,250,300,350,400,450,500,600,800,1500}, false, 1., false, false);

  config.addVariable(Config::BDTInput, "J_m", "m_{J} [GeV]", 0, 250, 5);
  config.addVariable(Config::BDTInput, "J_pt", "p_{T}^{J} [GeV]", 0, 800, 4);
  config.addVariable(Config::BDTInput, "J_d2", "D2", 0, 5, 4);

  config.addVariable(Config::BDTInput, "jj_m", "m_{jj} [GeV]", 0, 500, 5);
  config.addVariable(Config::BDTInput, "jj_pt", "p_{T}^{jj} [GeV]", 0, 800, 4);
  config.addVariable(Config::BDTInput, "jj_dPhi", "#Delta#phi(j1,j2) [rad]", 0, 5, 2);
  config.addVariable(Config::BDTInput, "jj_dR", "#DeltaR(j1,j2) [rad]", 0, 5, 2);

 // config.addWindowBlinding("J_m","0tag1pfat0pjet_0ptv_0lep_SR_baseline", "", 70,110);
 // config.addWindowBlinding("jj_m","0tag0pfat0pjet_0ptv_0lep_SR_baseline", "", 70,110);

  //////////////
  // REGIONS
  //////////////

  config.clearRegions();

  // merged
  // config.addRegion("0tag1pfat0pjet_0ptv_0lep_SR_baseline","0 lepton merged 0 tag","");
  // config.addRegion("1tag1pfat0pjet_0ptv_0lep_SR_baseline","0 lepton merged 1 tag","");
  // config.addRegion("2tag1pfat0pjet_0ptv_0lep_SR_baseline","0 lepton merged 2 tag","");

  config.addRegion("0tag1pfat0pjet_0ptv_0lep_SR_SubstrPassMassPass","0 lepton signal region merged 0 tag","");
  config.addRegion("1tag1pfat0pjet_0ptv_0lep_SR_SubstrPassMassPass","0 lepton signal region merged 1 tag","");
  config.addRegion("2tag1pfat0pjet_0ptv_0lep_SR_MassPass","0 lepton signal region merged 2 tag","");

  config.addRegion("0tag1pfat0pjet_0ptv_0lep_SR_SubstrFailMassPass","0 lepton low purity signal region merged 0 tag","");
  config.addRegion("1tag1pfat0pjet_0ptv_0lep_SR_SubstrFailMassPass","0 lepton low purity signal region merged 1 tag","");

  config.addRegion("0tag1pfat0pjet_0ptv_0lep_SR_SubstrPassMassFailHigh","0 lepton upper side-bands merged 0 tag","");
  config.addRegion("1tag1pfat0pjet_0ptv_0lep_SR_SubstrPassMassFailHigh","0 lepton upper side-bands merged 1 tag","");
  config.addRegion("2tag1pfat0pjet_0ptv_0lep_SR_MassFailHigh","0 lepton side-bands merged 2 tag","");

  config.addRegion("0tag1pfat0pjet_0ptv_0lep_SR_SubstrFailMassFailHigh","0 lepton low purity upper side-bands merged 0 tag","");
  config.addRegion("1tag1pfat0pjet_0ptv_0lep_SR_SubstrFailMassFailHigh","0 lepton low purity upper side-bands merged 1 tag","");

  // // resolved
  // config.addRegion("0tag0pfat0pjet_0ptv_0lep_SR_baseline","0 lepton resolved 0 tag","");
  // config.addRegion("1tag0pfat0pjet_0ptv_0lep_SR_baseline","0 lepton resolved 1 tag","");
  // config.addRegion("2tag0pfat0pjet_0ptv_0lep_SR_baseline","0 lepton resolved 2 tag","");

  config.addRegion("0tag0pfat0pjet_0ptv_0lep_SR_MassPass","0 lepton signal region resolved 0 tag","");
  config.addRegion("1tag0pfat0pjet_0ptv_0lep_SR_MassPass","0 lepton signal region resolved 1 tag","");
  config.addRegion("2tag0pfat0pjet_0ptv_0lep_SR_MassPass","0 lepton signal region resolved 2 tag","");

  config.addRegion("0tag0pfat0pjet_0ptv_0lep_SR_MassFailHigh","0 lepton upper side-bands resolved 0 tag","");
  config.addRegion("1tag0pfat0pjet_0ptv_0lep_SR_MassFailHigh","0 lepton upper side-bands resolved 1 tag","");
  config.addRegion("2tag0pfat0pjet_0ptv_0lep_SR_MassFailHigh","0 lepton upper side-bands resolved 2 tag","");


  ///////////////////////
  // SYSTEMATICS
  ///////////////////////

  config.clearSystematics();
  if (doSystematics) {
    // set systematics directory (contains all systematic variations)
    config.setSystematicDir("Systematics");

    // background modeling systematics
    config.addSystematic("SysWPtV", false);
    config.addSystematic("SysZPtV", false);
    config.addSystematic("SysTTbarPTV", false);
    config.addSystematic("SysVVPTVME", false);
    config.addSystematic("SysVVPTVPSUE", false);
    config.addSystematic("SysStoptPTV", false);
    config.addSystematic("SysStopWtPTV", false);

    config.addSystematic("SysMbbWShapeOnly", false);
    config.addSystematic("SysMbbZShapeOnly", false);
    config.addSystematic("SysMbbTTbarShapeOnly", false);
    config.addSystematic("SysMbbStoptShapeOnly", false);
    config.addSystematic("SysMbbStopWtShapeOnly", false);
    config.addSystematic("SysMbbVVMEShapeOnly", false);
    config.addSystematic("SysMbbVVPSUEShapeOnly", false);

    // pile up reweighting
    config.addSystematic("SysPRW_DATASF", false);

    // MET trigger SF
    config.addSystematic("SysMETTrigStat", false);
    config.addSystematic("SysMETTrigSyst", false);

    // large-R jet systematics
    config.addSystematic("SysFATJET_D2R", true);
    config.addSystematic("SysFATJET_JMR", true);
    config.addSystematic("SysFATJET_JER", true);
    //
    // use medium correlated nuisance parameter scheme (choose one!)
    // config.addSystematic("SysFATJET_Medium_JET_Comb_Baseline_Kin", false);
    // config.addSystematic("SysFATJET_Medium_JET_Comb_Modelling_Kin", false);
    // config.addSystematic("SysFATJET_Medium_JET_Comb_TotalStat_Kin", false);
    // config.addSystematic("SysFATJET_Medium_JET_Comb_Tracking_Kin", false);
    //
    // use weakly correlated nuisance parameter scheme (choose one!)
    config.addSystematic("SysFATJET_Weak_JET_Rtrk_Baseline_mass", false);
    config.addSystematic("SysFATJET_Weak_JET_Rtrk_Modelling_mass", false);
    config.addSystematic("SysFATJET_Weak_JET_Rtrk_TotalStat_mass", false);
    config.addSystematic("SysFATJET_Weak_JET_Rtrk_Tracking_mass", false);
    config.addSystematic("SysFATJET_Weak_JET_Rtrk_Baseline_D2Beta1", false);
    config.addSystematic("SysFATJET_Weak_JET_Rtrk_Modelling_D2Beta1", false);
    config.addSystematic("SysFATJET_Weak_JET_Rtrk_TotalStat_D2Beta1", false);
    config.addSystematic("SysFATJET_Weak_JET_Rtrk_Tracking_D2Beta1", false);
    config.addSystematic("SysFATJET_Weak_JET_Rtrk_Baseline_pT", false);
    config.addSystematic("SysFATJET_Weak_JET_Rtrk_Modelling_pT", false);
    config.addSystematic("SysFATJET_Weak_JET_Rtrk_TotalStat_pT", false);
    config.addSystematic("SysFATJET_Weak_JET_Rtrk_Tracking_pT", false);

    // lepton systematics
    config.addSystematic("SysMUONS_MS", false);
    config.addSystematic("SysMUONS_ID", false);
    config.addSystematic("SysMUON_SCALE", false);
    config.addSystematic("SysMUON_TTVA_SYS", false);
    config.addSystematic("SysMUON_TTVA_STAT", false);
    config.addSystematic("SysMUON_ISO_SYS", false);
    config.addSystematic("SysMUON_ISO_STAT", false);
    config.addSystematic("SysMUON_EFF_SYS", false);
    config.addSystematic("SysMUON_EFF_STAT", false);
    // muon low pt systematics are not used
    // config.addSystematic("SysMUON_EFF_SYS_LOWPT", false);
    // config.addSystematic("SysMUON_EFF_STAT_LOWPT", false);

    config.addSystematic("SysEG_SCALE_ALL", false);
    config.addSystematic("SysEG_RESOLUTION_ALL", false);
    config.addSystematic("SysEL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR", false);
    config.addSystematic("SysEL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR", false);
    config.addSystematic("SysEL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR", false);
    config.addSystematic("SysEL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR", false);

    // jet systematics
    config.addSystematic("SysJET_JER_SINGLE_NP", true);
    config.addSystematic("SysJET_JvtEfficiency", false);
    //
    // strongly reduced scheme (choose one!)
    config.addSystematic("SysJET_SR1_JET_EtaIntercalibration_NonClosure", false);
    config.addSystematic("SysJET_SR1_JET_GroupedNP_3", false);
    config.addSystematic("SysJET_SR1_JET_GroupedNP_2", false);
    config.addSystematic("SysJET_SR1_JET_GroupedNP_1", false);
    
    // 21 NP scheme (choose one!)
    // config.addSystematic("SysJET_21NP_JET_BJES_Response", false);
    // config.addSystematic("SysJET_21NP_JET_EffectiveNP_1", false);
    // config.addSystematic("SysJET_21NP_JET_EffectiveNP_2", false);
    // config.addSystematic("SysJET_21NP_JET_EffectiveNP_3", false);
    // config.addSystematic("SysJET_21NP_JET_EffectiveNP_4", false);
    // config.addSystematic("SysJET_21NP_JET_EffectiveNP_5", false);
    // config.addSystematic("SysJET_21NP_JET_EffectiveNP_6", false);
    // config.addSystematic("SysJET_21NP_JET_EffectiveNP_7", false);
    // config.addSystematic("SysJET_21NP_JET_EffectiveNP_8restTerm", false);
    // config.addSystematic("SysJET_21NP_JET_EtaIntercallibration_Modelling", false);
    // config.addSystematic("SysJET_21NP_JET_JET_EtaIntercallibration_NonClosure", false);
    // config.addSystematic("SysJET_21NP_JET_JET_EtaIntercallibration_TotalStat", false);
    // config.addSystematic("SysJET_21NP_JET_Flavor_Response", false);
    // config.addSystematic("SysJET_21NP_JET_Flavor_Composition", false);
    // config.addSystematic("SysJET_21NP_JET_Pileup_OffsetMu", false);
    // config.addSystematic("SysJET_21NP_JET_Pileup_PtTerm", false);
    // config.addSystematic("SysJET_21NP_JET_Pileup_RhoTopology", false);
    // config.addSystematic("SysJET_21NP_JET_PunchThrough_MC15", false);
    // config.addSystematic("SysJET_21NP_JET_SingleParticle_HighPt", false);


    // MET systematics
    config.addSystematic("SysMET_JetTrk_Scale", false);
    config.addSystematic("SysMET_SoftTrk_Scale", false);
    config.addSystematic("SysMET_SoftTrk_ResoPara", true);
    config.addSystematic("SysMET_SoftTrk_ResoPerp", true);

    // b-tagging systematics
    config.addSystematic("SysFT_EFF_extrapolation_from_charm_AntiKt2PV0TrackJets", false);
    config.addSystematic("SysFT_EFF_extrapolation_AntiKt2PV0TrackJets", false);
    config.addSystematic("SysFT_EFF_Eigen_Light_4_AntiKt2PV0TrackJets", false);
    config.addSystematic("SysFT_EFF_Eigen_Light_3_AntiKt2PV0TrackJets", false);
    config.addSystematic("SysFT_EFF_Eigen_Light_2_AntiKt2PV0TrackJets", false);
    config.addSystematic("SysFT_EFF_Eigen_Light_1_AntiKt2PV0TrackJets", false);
    config.addSystematic("SysFT_EFF_Eigen_Light_0_AntiKt2PV0TrackJets", false);
    config.addSystematic("SysFT_EFF_Eigen_C_3_AntiKt2PV0TrackJets", false);
    config.addSystematic("SysFT_EFF_Eigen_C_2_AntiKt2PV0TrackJets", false);
    config.addSystematic("SysFT_EFF_Eigen_C_1_AntiKt2PV0TrackJets", false);
    config.addSystematic("SysFT_EFF_Eigen_C_0_AntiKt2PV0TrackJets", false);
    config.addSystematic("SysFT_EFF_Eigen_B_2_AntiKt2PV0TrackJets", false);
    config.addSystematic("SysFT_EFF_Eigen_B_1_AntiKt2PV0TrackJets", false);
    config.addSystematic("SysFT_EFF_Eigen_B_0_AntiKt2PV0TrackJets", false);
    config.addSystematic("SysFT_EFF_extrapolation_from_charm_AntiKt4EMTopoJets", false);
    config.addSystematic("SysFT_EFF_extrapolation_AntiKt4EMTopoJets", false);
    config.addSystematic("SysFT_EFF_Eigen_Light_4_AntiKt4EMTopoJets", false);
    config.addSystematic("SysFT_EFF_Eigen_Light_3_AntiKt4EMTopoJets", false);
    config.addSystematic("SysFT_EFF_Eigen_Light_2_AntiKt4EMTopoJets", false);
    config.addSystematic("SysFT_EFF_Eigen_Light_1_AntiKt4EMTopoJets", false);
    config.addSystematic("SysFT_EFF_Eigen_Light_0_AntiKt4EMTopoJets", false);
    config.addSystematic("SysFT_EFF_Eigen_C_3_AntiKt4EMTopoJets", false);
    config.addSystematic("SysFT_EFF_Eigen_C_2_AntiKt4EMTopoJets", false);
    config.addSystematic("SysFT_EFF_Eigen_C_1_AntiKt4EMTopoJets", false);
    config.addSystematic("SysFT_EFF_Eigen_C_0_AntiKt4EMTopoJets", false);
    config.addSystematic("SysFT_EFF_Eigen_B_2_AntiKt4EMTopoJets", false);
    config.addSystematic("SysFT_EFF_Eigen_B_1_AntiKt4EMTopoJets", false);
    config.addSystematic("SysFT_EFF_Eigen_B_0_AntiKt4EMTopoJets", false);

    // multijet shape variation systematics
    config.addSystematic("SysMJ_Shape0mergedMassPass", false);
    config.addSystematic("SysMJ_Shape1mergedMassPass", false);
    config.addSystematic("SysMJ_Shape2mergedMassPass", false);
    config.addSystematic("SysMJ_Shape0resolvedMassPass", false);
    config.addSystematic("SysMJ_Shape1resolvedMassPass", false);
    config.addSystematic("SysMJ_Shape2resolvedMassPass", false);
    config.addSystematic("SysMJ_Shape0mergedMassFail", false);
    config.addSystematic("SysMJ_Shape1mergedMassFail", false);
    config.addSystematic("SysMJ_Shape2mergedMassFail", false);
    config.addSystematic("SysMJ_Shape0resolvedMassFail", false);
    config.addSystematic("SysMJ_Shape1resolvedMassFail", false);
    config.addSystematic("SysMJ_Shape2resolvedMassFail", false);
  }


  // // create systematic variation plots ("up/down" variation plots)
  // if(doSystematics && addSystematicVariationPlots)
  // {
  //   // inclusive
  // config.addDetailedSystematicsPlots("MET", "", "0tag1pfat0pjet_0ptv_0lep_SR_baseline","0 lepton merged 0 tag");
  // config.addDetailedSystematicsPlots("MET", "", "1tag1pfat0pjet_0ptv_0lep_SR_baseline","0 lepton merged 1 tag");
  // config.addDetailedSystematicsPlots("MET", "", "2tag1pfat0pjet_0ptv_0lep_SR_baseline","0 lepton merged 2 tag");
  // config.addDetailedSystematicsPlots("MET", "", "0tag0pfat0pjet_0ptv_0lep_SR_baseline","0 lepton resolved 0 tag");
  // config.addDetailedSystematicsPlots("MET", "", "1tag0pfat0pjet_0ptv_0lep_SR_baseline","0 lepton resolved 1 tag");
  // config.addDetailedSystematicsPlots("MET", "", "2tag0pfat0pjet_0ptv_0lep_SR_baseline","0 lepton resolved 2 tag");

  // // mass window
  // config.addDetailedSystematicsPlots("MET", "", "0tag1pfat0pjet_0ptv_0lep_SR_SubstPassMassPass","0 lepton merged 0 tag SR");
  // config.addDetailedSystematicsPlots("MET", "", "0tag1pfat0pjet_0ptv_0lep_SR_SubstPassMassPass","0 lepton merged 1 tag SR");
  // config.addDetailedSystematicsPlots("MET", "", "0tag1pfat0pjet_0ptv_0lep_SR_MassPass","0 lepton merged 2 tag SR");
  // config.addDetailedSystematicsPlots("MET", "", "0tag1pfat0pjet_0ptv_0lep_SR_SubstFailMassPass","0 lepton merged 0 tag LP SR");
  // config.addDetailedSystematicsPlots("MET", "", "0tag1pfat0pjet_0ptv_0lep_SR_SubstFailMassPass","0 lepton merged 1 tag LP SR");
  // config.addDetailedSystematicsPlots("MET", "", "0tag0pfat0pjet_0ptv_0lep_SR_MassPass","0 lepton resolved 0 tag SR");
  // config.addDetailedSystematicsPlots("MET", "", "0tag0pfat0pjet_0ptv_0lep_SR_MassPass","0 lepton resolved 1 tag SR");
  // config.addDetailedSystematicsPlots("MET", "", "0tag0pfat0pjet_0ptv_0lep_SR_MassPass","0 lepton resolved 2 tag SR");

  // // side-bands
  // config.addDetailedSystematicsPlots("MET", "", "0tag1pfat0pjet_0ptv_0lep_SR_SubstrPassMassFail","0 lepton merged 0 tag side-band");
  // config.addDetailedSystematicsPlots("MET", "", "1tag1pfat0pjet_0ptv_0lep_SR_SubstrPassMassFail","0 lepton merged 1 tag side-band");
  // config.addDetailedSystematicsPlots("MET", "", "2tag1pfat0pjet_0ptv_0lep_SR_MassFail","0 lepton merged 2 tag side-band");
  // config.addDetailedSystematicsPlots("MET", "", "0tag0pfat0pjet_0ptv_0lep_SR_MassFail","0 lepton resolved 0 tag side-band");
  // config.addDetailedSystematicsPlots("MET", "", "1tag0pfat0pjet_0ptv_0lep_SR_MassFail","0 lepton resolved 1 tag side-band");
  // config.addDetailedSystematicsPlots("MET", "", "2tag0pfat0pjet_0ptv_0lep_SR_MassFail","0 lepton resolved 2 tag side-band");

  // }

  ///////////////////////////
  /// Now ready to make plots

  // check for errors in configuration
  if(!config.isValid()) {
    std::cout << "Error in configuration: ===> Aborting..." << std::endl;
    return;
  }

  // we have all what we need to know
  // ==> now make the plots
  PlotMaker plotter;
  plotter.makePlots(config);
}
