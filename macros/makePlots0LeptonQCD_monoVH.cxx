////////////////////////////////////////////
//makePlots0Lepton_monoVH
//
// This is the script used to make the bare minimum number of plots
// for the monoV analysis.  There are many additional features of this plotting
// package that a good physicist would explore by reading the code ... GET SOME!
//
////////////////////////////////////////////

#include "PlottingTool/Config.h"
#include "PlottingTool/PlotMaker.h"

#include <TROOT.h>

#include <iostream>
#include <string>
#include <vector>
#include <ctime>

using namespace std;

std::string getDate(){
  time_t rawtime;
  struct tm * timeinfo;
  char buffer[80];

  time (&rawtime);
  timeinfo = localtime(&rawtime);

  strftime(buffer,80,"%Y-%m-%d",timeinfo);
  std::string str(buffer);

  return str;
}

void makePlots0LeptonQCD_monoVH(std::string InputPath="./", std::string InputFile="inputsFile") {

  Config config;

  // should we overwrite existing plots or only do missing ones?
  config.setOverwrite(true);

  // path to the input file: can be local or on eos
  config.setInputPath(InputPath);

  // Since your input file name might not follow the conventions above,
  // it can be overwritten using the line below
  // Note: specify the name WITHOUT ".root" at the end
  config.setInputFile(InputFile);

  // If input histograms are stored in a subdirectory of the input file, specify below
  // config.setInputTDirectory("OneLepton");

  ////////////////
  // General info
  ////////////////

  config.setAnalysis( Config::ZeroLepton,	// Type = ZeroLepton, OneLepton or TwoLepton: Appears on the plots
		      "Internal",		// Status: Appear on the plots next to ATLAS label
		      "Moriond2016",		// Name = "LHCP2013": only useful for the above naming convention
		      "13",			// Center of mass energy: Appears on the plots
		      "36.1",			// Integrated luminosity: Appears on the plots
		      "125",			// Mass of the generated higgs boson: only useful for the above naming convention
		      "MPI",			// Name of your institute: only usefule for the above naming convention
		      "fram15");		// Input file version: only useful for the above naming convention

  config.setAnalysisTitle("monoV");

  /////////////////
  // OUTPUT PATH - relative to current directory
  /////////////////

  // config.setOutputPath("./plots0LeptonQCD");

  std::string directory = "/ptmp/mpp/pgadow/monoV/Plots/"+getDate()+"/"+getDate()+"plots0LeptonQCD/";
  std::string command = ".!mkdir -p " + directory;
  gROOT->ProcessLine(command.c_str());
  config.setOutputPath(directory);
  std::cout << "Output will be stored in " << directory << std::endl;

  /////////////////
  // SCALE FACTORS
  /////////////////
  //std::string scaleFactorsTag = "SF_0L_nJ_1128";
  //std::string scaleFactorsTag = "data15Only";
  //config.readScaleFactors("PlottingTool/scaleFactors/"+scaleFactorsTag+".txt");

  /////////////////////
  // SAMPLES
  /////////////////////

  config.addDataSample("data", "Data", 1);

  config.addBackgroundSample("WW",      "Diboson", kGray+1);
  config.addBackgroundSample("WZ",      "Diboson", kGray+1);
  config.addBackgroundSample("ZZ",      "Diboson", kGray+1);

  config.addBackgroundSample("W",      "W (inclusive)", kGreen-10);
  config.addBackgroundSample("Wl",     "W+ll", kGreen-9);
  config.addBackgroundSample("Wcl",    "W+cl", kGreen-6);
  config.addBackgroundSample("Wbb",    "W+(bb,bc,bl,cc)", kGreen+3);
  config.addBackgroundSample("Wbc",    "W+(bb,bc,bl,cc)", kGreen+3);
  config.addBackgroundSample("Wbl",    "W+(bb,bc,bl,cc)", kGreen+3);
  config.addBackgroundSample("Wcc",    "W+(bb,bc,bl,cc)", kGreen+3);

  config.addBackgroundSample("Z",      "Z (inclusive)", kAzure-10);
  config.addBackgroundSample("Zl",     "Z+ll", kAzure-9);
  config.addBackgroundSample("Zcl",    "Z+cl", kAzure-8);
  config.addBackgroundSample("Zbb",    "Z+(bb,bc,bl,cc)", kAzure+2);
  config.addBackgroundSample("Zbc",    "Z+(bb,bc,bl,cc)", kAzure+2);
  config.addBackgroundSample("Zbl",    "Z+(bb,bc,bl,cc)", kAzure+2);
  config.addBackgroundSample("Zcc",    "Z+(bb,bc,bl,cc)", kAzure+2);

  config.addBackgroundSample("stops",  "Single top", kOrange-1);
  config.addBackgroundSample("stopt",  "Single top", kOrange-1);
  config.addBackgroundSample("stopWt", "Single top", kOrange-1);

  config.addBackgroundSample("ttbar",   "t#bar{t}", kOrange);
  // config.addBackgroundSample("dijetJZW", "Dijet MC", kPink+1);
  // config.addBackgroundSample("dijetJZ", "Dijet MC", kPink+1);


  //////////////
  // VARIABLES
  //////////////

  // config.addVariable(Config::BDTInput, "MET", "{E}_{T}^{miss} [GeV]", 150, 550, 100);
  // config.addVariable(Config::BDTInput, "MET", "E_{T}^{miss} [GeV]", {150,200,250,300,350,400,450,500,600,800,1500}, false, 1., false, false);
  // config.addVariable(Config::BDTInput, "J_m", "m_{J} [GeV]", 0, 250, 10);
  // config.addVariable(Config::BDTInput, "jj_m", "m_{jj} [GeV]", 0, 250, 10);

  // config.addVariable(Config::BDTInput, "nJetsWithMuon", "n_{jets with muon}", 0, 10, 1);
  //

  config.addVariable(Config::BDTInput, "jetmet_mindPhi", "min #Delta#phi(E_{T}^{miss}, jets)", 0, 4, 1);
  config.addVariable(Config::BDTInput, "metmpt_Phi", "#Delta#phi(E_{T}^{miss}, p_{T}^{miss})", 0, 4, 1);
  config.addVariable(Config::BDTInput, "metJ_Phi", "#Delta#phi(E_{T}^{miss}, J)", 0, 4, 1);
  config.addVariable(Config::BDTInput, "metjj_Phi", "#Delta#phi(E_{T}^{miss}, jj)", 0, 4, 1);

  // config.addVariable(Config::BDTInput, "MT", "M_{T} [GeV]", 0, 3000, 10);

  // config.addVariable(Config::BDTInput, "PThrust", "p-thrust", -4, 4, 2);
  // config.addVariable(Config::BDTInput, "Spherocity", "#Delta#phi(j,j)", 0, 1, 2);



  //////////////
  // REGIONS
  //////////////

  config.clearRegions();

  // Inclusive selection, baseline selection
  // mass peak
  // config.addRegion("0tag1pfat0pjet_0ptv_QCD_baseline_in_SubstrPassMassPass","W/Z mass window 0 lep merged 0tag (no anti-QCD cuts)","");
  // config.addRegion("1tag1pfat0pjet_0ptv_QCD_baseline_in_SubstrPassMassPass","W/Z mass window 0 lep merged 1tag (no anti-QCD cuts)","");
  // config.addRegion("2tag1pfat0pjet_0ptv_QCD_baseline_in_MassPass","W/Z mass window 0 lep merged 2tag (no anti-QCD cuts)","");

  // config.addRegion("0tag0pfat0pjet_0ptv_QCD_baseline_in_MassPass","W/Z mass window 0 lep resolved 0tag (no anti-QCD cuts)","");
  // config.addRegion("1tag0pfat0pjet_0ptv_QCD_baseline_in_MassPass","W/Z mass window 0 lep resolved 1tag (no anti-QCD cuts)","");
  // config.addRegion("2tag0pfat0pjet_0ptv_QCD_baseline_in_MassPass","W/Z mass window 0 lep resolved 2tag (no anti-QCD cuts)","");

  // upper side-band
  config.addRegion("0ptag1pfat0pjet_0ptv_QCD_baseline_in_SubstrPassMassFailHigh","Upper W/Z mass side-band 0 lep merged 0-2tag (no anti-QCD cuts)",""); // 
  // config.addRegion("0tag1pfat0pjet_0ptv_QCD_baseline_in_SubstrPassMassFailHigh", "Upper W/Z mass side-band 0 lep merged 0tag (no anti-QCD cuts)","");
  // config.addRegion("1tag1pfat0pjet_0ptv_QCD_baseline_in_SubstrPassMassFailHigh", "Upper W/Z mass side-band 0 lep merged 1tag (no anti-QCD cuts)","");
  // config.addRegion("2tag1pfat0pjet_0ptv_QCD_baseline_in_MassFailHigh",           "Upper W/Z mass side-band 0 lep merged 2tag (no anti-QCD cuts)","");

  config.addRegion("0ptag0pfat0pjet_0ptv_QCD_baseline_in_MassFailHigh","Upper W/Z mass side-band 0 lep resolved 0-2tag (no anti-QCD cuts)","");
  // config.addRegion("0tag0pfat0pjet_0ptv_QCD_baseline_in_MassFailHigh", "Upper W/Z mass side-band 0 lep resolved 0tag (no anti-QCD cuts)","");
  // config.addRegion("1tag0pfat0pjet_0ptv_QCD_baseline_in_MassFailHigh", "Upper W/Z mass side-band 0 lep resolved 1tag (no anti-QCD cuts)","");
  // config.addRegion("2tag0pfat0pjet_0ptv_QCD_baseline_in_MassFailHigh", "Upper W/Z mass side-band 0 lep resolved 2tag (no anti-QCD cuts)","");



  // Inclusive selection, baseline + METJET selection
  // mass peak
  // config.addRegion("0tag1pfat0pjet_0ptv_QCD_dPhiJetMET_in_SubstrPassMassPass","W/Z mass window 0 lep merged 0tag baseline + dPhi(MET,V)","");
  // config.addRegion("1tag1pfat0pjet_0ptv_QCD_dPhiJetMET_in_SubstrPassMassPass","W/Z mass window 0 lep merged 1tag baseline + dPhi(MET,V)","");
  // config.addRegion("2tag1pfat0pjet_0ptv_QCD_dPhiJetMET_in_MassPass","W/Z mass window 0 lep merged 2tag baseline + dPhi(MET,V)","");

  // config.addRegion("0tag0pfat0pjet_0ptv_QCD_dPhiJetMET_in_MassPass","W/Z mass window 0 lep resolved 0tag baseline + dPhi(MET,V)","");
  // config.addRegion("1tag0pfat0pjet_0ptv_QCD_dPhiJetMET_in_MassPass","W/Z mass window 0 lep resolved 1tag baseline + dPhi(MET,V)","");
  // config.addRegion("2tag0pfat0pjet_0ptv_QCD_dPhiJetMET_in_MassPass","W/Z mass window 0 lep resolved 2tag baseline + dPhi(MET,V)","");

  // upper side-band
  config.addRegion("0ptag1pfat0pjet_0ptv_QCD_dPhiJetMET_in_SubstrPassMassFailHigh","Upper W/Z mass side-band 0 lep merged 0-2tag baseline + dPhi(MET,V)","");
  // config.addRegion("0tag1pfat0pjet_0ptv_QCD_dPhiJetMET_in_SubstrPassMassFailHigh","Upper W/Z mass side-band 0 lep merged 0tag baseline + dPhi(MET,V)","");
  // config.addRegion("1tag1pfat0pjet_0ptv_QCD_dPhiJetMET_in_SubstrPassMassFailHigh","Upper W/Z mass side-band 0 lep merged 1tag baseline + dPhi(MET,V)","");
  // config.addRegion("2tag1pfat0pjet_0ptv_QCD_dPhiJetMET_in_MassFailHigh",          "Upper W/Z mass side-band 0 lep merged 2tag baseline + dPhi(MET,V)","");

  config.addRegion("0ptag0pfat0pjet_0ptv_QCD_dPhiJetMET_in_MassFailHigh","Upper W/Z mass side-band 0 lep resolved 0-2tag baseline + dPhi(MET,V)","");
  // config.addRegion("0tag0pfat0pjet_0ptv_QCD_dPhiJetMET_in_MassFailHigh","Upper W/Z mass side-band 0 lep resolved 0tag baseline + dPhi(MET,V)","");
  // config.addRegion("1tag0pfat0pjet_0ptv_QCD_dPhiJetMET_in_MassFailHigh","Upper W/Z mass side-band 0 lep resolved 1tag baseline + dPhi(MET,V)","");
  // config.addRegion("2tag0pfat0pjet_0ptv_QCD_dPhiJetMET_in_MassFailHigh","Upper W/Z mass side-band 0 lep resolved 2tag baseline + dPhi(MET,V)","");



  // Inclusive selection, baseline + METMPT selection
  // mass peak
  // config.addRegion("0tag1pfat0pjet_0ptv_QCD_dPhiMETMPT_in_SubstrPassMassPass","W/Z mass window 0 lep merged 0tag baseline + dPhi(MET,MPT)","");
  // config.addRegion("1tag1pfat0pjet_0ptv_QCD_dPhiMETMPT_in_SubstrPassMassPass","W/Z mass window 0 lep merged 1tag baseline + dPhi(MET,MPT)","");
  // config.addRegion("2tag1pfat0pjet_0ptv_QCD_dPhiMETMPT_in_MassPass","W/Z mass window 0 lep merged 2tag baseline + dPhi(MET,MPT)","");

  // config.addRegion("0tag0pfat0pjet_0ptv_QCD_dPhiMETMPT_in_MassPass","W/Z mass window 0 lep resolved 0tag baseline + dPhi(MET,MPT)","");
  // config.addRegion("1tag0pfat0pjet_0ptv_QCD_dPhiMETMPT_in_MassPass","W/Z mass window 0 lep resolved 1tag baseline + dPhi(MET,MPT)","");
  // config.addRegion("2tag0pfat0pjet_0ptv_QCD_dPhiMETMPT_in_MassPass","W/Z mass window 0 lep resolved 2tag baseline + dPhi(MET,MPT)","");

  // side-bands
  config.addRegion("0ptag1pfat0pjet_0ptv_QCD_dPhiMETMPT_in_SubstrPassMassFailHigh","Upper W/Z mass side-band 0 lep merged 0-2tag baseline + dPhi(MET,MPT)","");
  // config.addRegion("0tag1pfat0pjet_0ptv_QCD_dPhiMETMPT_in_SubstrPassMassFailHigh","Upper W/Z mass side-band 0 lep merged 0tag baseline + dPhi(MET,MPT)","");
  // config.addRegion("1tag1pfat0pjet_0ptv_QCD_dPhiMETMPT_in_SubstrPassMassFailHigh","Upper W/Z mass side-band 0 lep merged 1tag baseline + dPhi(MET,MPT)","");
  // config.addRegion("2tag1pfat0pjet_0ptv_QCD_dPhiMETMPT_in_MassFailHigh",          "Upper W/Z mass side-band 0 lep merged 2tag baseline + dPhi(MET,MPT)","");

  config.addRegion("0ptag0pfat0pjet_0ptv_QCD_dPhiMETMPT_in_MassFailHigh","Upper W/Z mass side-band 0 lep resolved 0tag baseline + dPhi(MET,MPT)","");
  // config.addRegion("0tag0pfat0pjet_0ptv_QCD_dPhiMETMPT_in_MassFailHigh","Upper W/Z mass side-band 0 lep resolved 0tag baseline + dPhi(MET,MPT)","");
  // config.addRegion("1tag0pfat0pjet_0ptv_QCD_dPhiMETMPT_in_MassFailHigh","Upper W/Z mass side-band 0 lep resolved 1tag baseline + dPhi(MET,MPT)","");
  // config.addRegion("2tag0pfat0pjet_0ptv_QCD_dPhiMETMPT_in_MassFailHigh","Upper W/Z mass side-band 0 lep resolved 2tag baseline + dPhi(MET,MPT)","");


  // Inclusive selection, full selection
  // mass peak
  // config.addRegion("0tag1pfat0pjet_0ptv_QCD_selection_in_SubstrPassMassPass","W/Z mass window 0 lep merged 0tag (no min #Delta #Phi(jets, MET) cut)","");
  // config.addRegion("1tag1pfat0pjet_0ptv_QCD_selection_in_SubstrPassMassPass","W/Z mass window 0 lep merged 1tag (no min #Delta #Phi(jets, MET) cut)","");
  // config.addRegion("2tag1pfat0pjet_0ptv_QCD_selection_in_MassPass","W/Z mass window 0 lep merged 2tag (no min #Delta #Phi(jets, MET) cut)","");

  // config.addRegion("0tag0pfat0pjet_0ptv_QCD_selection_in_MassPass","W/Z mass window 0 lep resolved 0tag (no min #Delta #Phi(jets, MET) cut)","");
  // config.addRegion("1tag0pfat0pjet_0ptv_QCD_selection_in_MassPass","W/Z mass window 0 lep resolved 1tag (no min #Delta #Phi(jets, MET) cut)","");
  // config.addRegion("2tag0pfat0pjet_0ptv_QCD_selection_in_MassPass","W/Z mass window 0 lep resolved 2tag (no min #Delta #Phi(jets, MET) cut)","");

  // upper side-band
  config.addRegion("0ptag1pfat0pjet_0ptv_QCD_selection_in_SubstrPassMassFailHigh","Upper W/Z mass side-band 0 lep merged 0-2tag (no min #Delta #Phi(jets, MET) cut)","");
  // config.addRegion("0tag1pfat0pjet_0ptv_QCD_selection_in_SubstrPassMassFailHigh","Upper W/Z mass side-band 0 lep merged 0tag (no min #Delta #Phi(jets, MET) cut)","");
  // config.addRegion("1tag1pfat0pjet_0ptv_QCD_selection_in_SubstrPassMassFailHigh","Upper W/Z mass side-band 0 lep merged 1tag (no min #Delta #Phi(jets, MET) cut)","");
  // config.addRegion("2tag1pfat0pjet_0ptv_QCD_selection_in_MassFailHigh",          "Upper W/Z mass side-band 0 lep merged 2tag (no min #Delta #Phi(jets, MET) cut)","");

  config.addRegion("0ptag0pfat0pjet_0ptv_QCD_selection_in_MassFailHigh","Upper W/Z mass side-band 0 lep resolved 0-2tag (no min #Delta #Phi(jets, MET) cut)","");
  // config.addRegion("0tag0pfat0pjet_0ptv_QCD_selection_in_MassFailHigh","Upper W/Z mass side-band 0 lep resolved 0tag (no min #Delta #Phi(jets, MET) cut)","");
  // config.addRegion("1tag0pfat0pjet_0ptv_QCD_selection_in_MassFailHigh","Upper W/Z mass side-band 0 lep resolved 1tag (no min #Delta #Phi(jets, MET) cut)","");
  // config.addRegion("2tag0pfat0pjet_0ptv_QCD_selection_in_MassFailHigh","Upper W/Z mass side-band 0 lep resolved 2tag (no min #Delta #Phi(jets, MET) cut)","");



  // QCD enriched region, baseline selection
  // mass peak
  // config.addRegion("0tag1pfat0pjet_0ptv_QCD_baseline_cr_SubstrPassMassPass","QCD enriched W/Z mass window 0 lep merged 0tag","");
  // config.addRegion("1tag1pfat0pjet_0ptv_QCD_baseline_cr_SubstrPassMassPass","QCD enriched W/Z mass window 0 lep merged 1tag","");
  // config.addRegion("2tag1pfat0pjet_0ptv_QCD_baseline_cr_MassPass","QCD enriched W/Z mass window 0 lep merged 2tag","");

  // config.addRegion("0tag0pfat0pjet_0ptv_QCD_baseline_cr_MassPass","QCD enriched W/Z mass window 0 lep resolved 0tag","");
  // config.addRegion("1tag0pfat0pjet_0ptv_QCD_baseline_cr_MassPass","QCD enriched W/Z mass window 0 lep resolved 1tag","");
  // config.addRegion("2tag0pfat0pjet_0ptv_QCD_baseline_cr_MassPass","QCD enriched W/Z mass window 0 lep resolved 2tag","");

  // upper side-band
  // config.addRegion("0ptag1pfat0pjet_0ptv_QCD_baseline_cr_SubstrPassMassFailHigh","QCD enriched Upper W/Z mass side-band 0 lep merged 0-2tag","");
  // config.addRegion("0tag1pfat0pjet_0ptv_QCD_baseline_cr_SubstrPassMassFailHigh","QCD enriched Upper W/Z mass side-band 0 lep merged 0tag","");
  // config.addRegion("1tag1pfat0pjet_0ptv_QCD_baseline_cr_SubstrPassMassFailHigh","QCD enriched Upper W/Z mass side-band 0 lep merged 1tag","");
  // config.addRegion("2tag1pfat0pjet_0ptv_QCD_baseline_cr_MassFailHigh","QCD enriched Upper W/Z mass side-band 0 lep merged 2tag","");

  // config.addRegion("0ptag0pfat0pjet_0ptv_QCD_baseline_cr_MassFailHigh","QCD enriched Upper W/Z mass side-band 0 lep resolved 0-2tag","");
  // config.addRegion("0tag0pfat0pjet_0ptv_QCD_baseline_cr_MassFailHigh","QCD enriched Upper W/Z mass side-band 0 lep resolved 0tag","");
  // config.addRegion("1tag0pfat0pjet_0ptv_QCD_baseline_cr_MassFailHigh","QCD enriched Upper W/Z mass side-band 0 lep resolved 1tag","");
  // config.addRegion("2tag0pfat0pjet_0ptv_QCD_baseline_cr_MassFailHigh","QCD enriched Upper W/Z mass side-band 0 lep resolved 2tag","");

  // QCD suppressed region, baseline selection
  // mass peak
  // config.addRegion("0tag1pfat0pjet_0ptv_QCD_baseline_sr_SubstrPassMassPass","QCD suppressed W/Z mass window 0 lep merged 0tag","");
  // config.addRegion("1tag1pfat0pjet_0ptv_QCD_baseline_sr_SubstrPassMassPass","QCD suppressed W/Z mass window 0 lep merged 1tag","");
  // config.addRegion("2tag1pfat0pjet_0ptv_QCD_baseline_sr_MassPass","QCD suppressed W/Z mass window 0 lep merged 2tag","");

  // config.addRegion("0tag0pfat0pjet_0ptv_QCD_baseline_sr_MassPass","QCD suppressed W/Z mass window 0 lep resolved 0tag","");
  // config.addRegion("1tag0pfat0pjet_0ptv_QCD_baseline_sr_MassPass","QCD suppressed W/Z mass window 0 lep resolved 1tag","");
  // config.addRegion("2tag0pfat0pjet_0ptv_QCD_baseline_sr_MassPass","QCD suppressed W/Z mass window 0 lep resolved 2tag","");

  // upper side-band
  config.addRegion("0ptag1pfat0pjet_0ptv_QCD_baseline_sr_SubstrPassMassFailHigh","QCD suppressed Upper W/Z mass side-band 0 lep merged 0-2tag","");
  // config.addRegion("0tag1pfat0pjet_0ptv_QCD_baseline_sr_SubstrPassMassFailHigh","QCD suppressed Upper W/Z mass side-band 0 lep merged 0tag","");
  // config.addRegion("1tag1pfat0pjet_0ptv_QCD_baseline_sr_SubstrPassMassFailHigh","QCD suppressed Upper W/Z mass side-band 0 lep merged 1tag","");
  // config.addRegion("2tag1pfat0pjet_0ptv_QCD_baseline_sr_MassFailHigh","QCD suppressed Upper W/Z mass side-band 0 lep merged 2tag","");

  config.addRegion("0ptag0pfat0pjet_0ptv_QCD_baseline_sr_MassFailHigh","QCD suppressed Upper W/Z mass side-band 0 lep resolved 0-2tag","");
  // config.addRegion("0tag0pfat0pjet_0ptv_QCD_baseline_sr_MassFailHigh","QCD suppressed Upper W/Z mass side-band 0 lep resolved 0tag","");
  // config.addRegion("1tag0pfat0pjet_0ptv_QCD_baseline_sr_MassFailHigh","QCD suppressed Upper W/Z mass side-band 0 lep resolved 1tag","");
  // config.addRegion("2tag0pfat0pjet_0ptv_QCD_baseline_sr_MassFailHigh","QCD suppressed Upper W/Z mass side-band 0 lep resolved 2tag","");




  // QCD enriched region, dPhiJetMET selection
  // mass peak
  // config.addRegion("0tag1pfat0pjet_0ptv_QCD_dPhiJetMET_cr_SubstrPassMassPass","QCD enriched W/Z mass window 0 lep merged 0tag + dPhi(MET,V) cut","");
  // config.addRegion("1tag1pfat0pjet_0ptv_QCD_dPhiJetMET_cr_SubstrPassMassPass","QCD enriched W/Z mass window 0 lep merged 1tag + dPhi(MET,V) cut","");
  // config.addRegion("2tag1pfat0pjet_0ptv_QCD_dPhiJetMET_cr_MassPass","QCD enriched W/Z mass window 0 lep merged 2tag + dPhi(MET,V) cut","");

  // config.addRegion("0tag0pfat0pjet_0ptv_QCD_dPhiJetMET_cr_MassPass","QCD enriched W/Z mass window 0 lep resolved 0tag + dPhi(MET,V) cut","");
  // config.addRegion("1tag0pfat0pjet_0ptv_QCD_dPhiJetMET_cr_MassPass","QCD enriched W/Z mass window 0 lep resolved 1tag + dPhi(MET,V) cut","");
  // config.addRegion("2tag0pfat0pjet_0ptv_QCD_dPhiJetMET_cr_MassPass","QCD enriched W/Z mass window 0 lep resolved 2tag + dPhi(MET,V) cut","");

  // upper side-band
  // config.addRegion("0tag1pfat0pjet_0ptv_QCD_dPhiJetMET_cr_SubstrPassMassFailHigh","QCD enriched upper W/Z mass side-bands 0 lep merged 0tag + dPhi(MET,V) cut","");
  // config.addRegion("1tag1pfat0pjet_0ptv_QCD_dPhiJetMET_cr_SubstrPassMassFailHigh","QCD enriched upper W/Z mass side-bands 0 lep merged 1tag + dPhi(MET,V) cut","");
  // config.addRegion("2tag1pfat0pjet_0ptv_QCD_dPhiJetMET_cr_MassFailHigh","QCD enriched upper W/Z mass side-bands 0 lep merged 2tag + dPhi(MET,V) cut","");

  // config.addRegion("0tag0pfat0pjet_0ptv_QCD_dPhiJetMET_cr_MassFailHigh","QCD enriched upper W/Z mass side-bands 0 lep resolved 0tag + dPhi(MET,V) cut","");
  // config.addRegion("1tag0pfat0pjet_0ptv_QCD_dPhiJetMET_cr_MassFailHigh","QCD enriched upper W/Z mass side-bands 0 lep resolved 1tag + dPhi(MET,V) cut","");
  // config.addRegion("2tag0pfat0pjet_0ptv_QCD_dPhiJetMET_cr_MassFailHigh","QCD enriched upper W/Z mass side-bands 0 lep resolved 2tag + dPhi(MET,V) cut","");

  // QCD suppressed region, dPhiJetMET selection
  // mass peak
  // config.addRegion("0tag1pfat0pjet_0ptv_QCD_dPhiJetMET_sr_SubstrPassMassPass","QCD suppressed upper W/Z mass window 0 lep merged 0tag + dPhi(MET,V) cut","");
  // config.addRegion("1tag1pfat0pjet_0ptv_QCD_dPhiJetMET_sr_SubstrPassMassPass","QCD suppressed upper W/Z mass window 0 lep merged 1tag + dPhi(MET,V) cut","");
  // config.addRegion("2tag1pfat0pjet_0ptv_QCD_dPhiJetMET_sr_MassPass","QCD suppressed upper W/Z mass window 0 lep merged 2tag + dPhi(MET,V) cut","");

  // config.addRegion("0tag0pfat0pjet_0ptv_QCD_dPhiJetMET_sr_MassPass","QCD suppressed upper W/Z mass window 0 lep resolved 0tag + dPhi(MET,V) cut","");
  // config.addRegion("1tag0pfat0pjet_0ptv_QCD_dPhiJetMET_sr_MassPass","QCD suppressed upper W/Z mass window 0 lep resolved 1tag + dPhi(MET,V) cut","");
  // config.addRegion("2tag0pfat0pjet_0ptv_QCD_dPhiJetMET_sr_MassPass","QCD suppressed upper W/Z mass window 0 lep resolved 2tag + dPhi(MET,V) cut","");

  // upper side-band
  config.addRegion("0ptag1pfat0pjet_0ptv_QCD_dPhiJetMET_sr_SubstrPassMassFailHigh","QCD suppressed upper W/Z mass side-bands 0 lep merged 0-2tag + dPhi(MET,V) cut","");
  // config.addRegion("0tag1pfat0pjet_0ptv_QCD_dPhiJetMET_sr_SubstrPassMassFailHigh","QCD suppressed upper W/Z mass side-bands 0 lep merged 0tag + dPhi(MET,V) cut","");
  // config.addRegion("1tag1pfat0pjet_0ptv_QCD_dPhiJetMET_sr_SubstrPassMassFailHigh","QCD suppressed upper W/Z mass side-bands 0 lep merged 1tag + dPhi(MET,V) cut","");
  // config.addRegion("2tag1pfat0pjet_0ptv_QCD_dPhiJetMET_sr_MassFailHigh","QCD suppressed upper W/Z mass side-bands 0 lep merged 2tag + dPhi(MET,V) cut","");

  config.addRegion("0ptag0pfat0pjet_0ptv_QCD_dPhiJetMET_sr_MassFailHigh","QCD suppressed upper W/Z mass side-bands 0 lep resolved 0-2tag + dPhi(MET,V) cut","");
  // config.addRegion("0tag0pfat0pjet_0ptv_QCD_dPhiJetMET_sr_MassFailHigh","QCD suppressed upper W/Z mass side-bands 0 lep resolved 0tag + dPhi(MET,V) cut","");
  // config.addRegion("1tag0pfat0pjet_0ptv_QCD_dPhiJetMET_sr_MassFailHigh","QCD suppressed upper W/Z mass side-bands 0 lep resolved 1tag + dPhi(MET,V) cut","");
  // config.addRegion("2tag0pfat0pjet_0ptv_QCD_dPhiJetMET_sr_MassFailHigh","QCD suppressed upper W/Z mass side-bands 0 lep resolved 2tag + dPhi(MET,V) cut","");





  // QCD enriched region, dPhiMETMPT selection
  // mass peak
  // config.addRegion("0tag1pfat0pjet_0ptv_QCD_dPhiMETMPT_cr_SubstrPassMassPass","QCD enriched upper W/Z mass window 0 lep merged 0tag + dPhi(MET,MPT) cut","");
  // config.addRegion("1tag1pfat0pjet_0ptv_QCD_dPhiMETMPT_cr_SubstrPassMassPass","QCD enriched upper W/Z mass window 0 lep merged 1tag + dPhi(MET,MPT) cut","");
  // config.addRegion("2tag1pfat0pjet_0ptv_QCD_dPhiMETMPT_cr_MassPass","QCD enriched upper W/Z mass window 0 lep merged 2tag + dPhi(MET,MPT) cut","");

  // config.addRegion("0tag0pfat0pjet_0ptv_QCD_dPhiMETMPT_cr_MassPass","QCD enriched upper W/Z mass window 0 lep resolved 0tag + dPhi(MET,MPT) cut","");
  // config.addRegion("1tag0pfat0pjet_0ptv_QCD_dPhiMETMPT_cr_MassPass","QCD enriched upper W/Z mass window 0 lep resolved 1tag + dPhi(MET,MPT) cut","");
  // config.addRegion("2tag0pfat0pjet_0ptv_QCD_dPhiMETMPT_cr_MassPass","QCD enriched upper W/Z mass window 0 lep resolved 2tag + dPhi(MET,MPT) cut","");

  // upper side-band
  // config.addRegion("0tag1pfat0pjet_0ptv_QCD_dPhiMETMPT_cr_SubstrPassMassFailHigh","QCD enriched upper W/Z mass side-bands 0 lep merged 0tag + dPhi(MET,MPT) cut","");
  // config.addRegion("1tag1pfat0pjet_0ptv_QCD_dPhiMETMPT_cr_SubstrPassMassFailHigh","QCD enriched upper W/Z mass side-bands 0 lep merged 1tag + dPhi(MET,MPT) cut","");
  // config.addRegion("2tag1pfat0pjet_0ptv_QCD_dPhiMETMPT_cr_MassFailHigh","QCD enriched upper W/Z mass side-bands 0 lep merged 2tag + dPhi(MET,MPT) cut","");

  // config.addRegion("0tag0pfat0pjet_0ptv_QCD_dPhiMETMPT_cr_MassFailHigh","QCD enriched upper W/Z mass side-bands 0 lep resolved 0tag + dPhi(MET,MPT) cut","");
  // config.addRegion("1tag0pfat0pjet_0ptv_QCD_dPhiMETMPT_cr_MassFailHigh","QCD enriched upper W/Z mass side-bands 0 lep resolved 1tag + dPhi(MET,MPT) cut","");
  // config.addRegion("2tag0pfat0pjet_0ptv_QCD_dPhiMETMPT_cr_MassFailHigh","QCD enriched upper W/Z mass side-bands 0 lep resolved 2tag + dPhi(MET,MPT) cut","");

  // QCD suppressed region, dPhiMETMPT selection
  // mass peak
  // config.addRegion("0tag1pfat0pjet_0ptv_QCD_dPhiMETMPT_sr_SubstrPassMassPass","QCD suppressed upper W/Z mass window 0 lep merged 0tag + dPhi(MET,MPT) cut","");
  // config.addRegion("1tag1pfat0pjet_0ptv_QCD_dPhiMETMPT_sr_SubstrPassMassPass","QCD suppressed upper W/Z mass window 0 lep merged 1tag + dPhi(MET,MPT) cut","");
  // config.addRegion("2tag1pfat0pjet_0ptv_QCD_dPhiMETMPT_sr_MassPass","QCD suppressed upper W/Z mass window 0 lep merged 2tag + dPhi(MET,MPT) cut","");

  // config.addRegion("0tag0pfat0pjet_0ptv_QCD_dPhiMETMPT_sr_MassPass","QCD suppressed upper W/Z mass window 0 lep resolved 0tag + dPhi(MET,MPT) cut","");
  // config.addRegion("1tag0pfat0pjet_0ptv_QCD_dPhiMETMPT_sr_MassPass","QCD suppressed upper W/Z mass window 0 lep resolved 1tag + dPhi(MET,MPT) cut","");
  // config.addRegion("2tag0pfat0pjet_0ptv_QCD_dPhiMETMPT_sr_MassPass","QCD suppressed upper W/Z mass window 0 lep resolved 2tag + dPhi(MET,MPT) cut","");

  // upper side-band
  config.addRegion("0ptag1pfat0pjet_0ptv_QCD_dPhiMETMPT_sr_SubstrPassMassFailHigh","QCD suppressed upper W/Z mass side-band 0 lep merged 0-2tag + dPhi(MET,MPT) cut","");
  // config.addRegion("0tag1pfat0pjet_0ptv_QCD_dPhiMETMPT_sr_SubstrPassMassFailHigh","QCD suppressed upper W/Z mass side-band 0 lep merged 0tag + dPhi(MET,MPT) cut","");
  // config.addRegion("1tag1pfat0pjet_0ptv_QCD_dPhiMETMPT_sr_SubstrPassMassFailHigh","QCD suppressed upper W/Z mass side-band 0 lep merged 1tag + dPhi(MET,MPT) cut","");
  // config.addRegion("2tag1pfat0pjet_0ptv_QCD_dPhiMETMPT_sr_MassFailHigh","QCD suppressed upper W/Z mass side-band 0 lep merged 2tag + dPhi(MET,MPT) cut","");

  config.addRegion("0ptag0pfat0pjet_0ptv_QCD_dPhiMETMPT_sr_MassFailHigh","QCD suppressed upper W/Z mass side-band 0 lep resolved 0-2tag + dPhi(MET,MPT) cut","");
  // config.addRegion("0tag0pfat0pjet_0ptv_QCD_dPhiMETMPT_sr_MassFailHigh","QCD suppressed upper W/Z mass side-band 0 lep resolved 0tag + dPhi(MET,MPT) cut","");
  // config.addRegion("1tag0pfat0pjet_0ptv_QCD_dPhiMETMPT_sr_MassFailHigh","QCD suppressed upper W/Z mass side-band 0 lep resolved 1tag + dPhi(MET,MPT) cut","");
  // config.addRegion("2tag0pfat0pjet_0ptv_QCD_dPhiMETMPT_sr_MassFailHigh","QCD suppressed upper W/Z mass side-band 0 lep resolved 2tag + dPhi(MET,MPT) cut","");




  // // QCD enriched region, full selection
  // // mass peak
  // config.addRegion("0tag1pfat0pjet_0ptv_QCD_selection_cr_SubstrPassMassPass","QCD CR W/Z mass window 0 lep merged 0tag","");
  // config.addRegion("1tag1pfat0pjet_0ptv_QCD_selection_cr_SubstrPassMassPass","QCD CR W/Z mass window 0 lep merged 1tag","");
  // config.addRegion("2tag1pfat0pjet_0ptv_QCD_selection_cr_MassPass","QCD CR W/Z mass window 0 lep merged 2tag","");

  // config.addRegion("0tag1pfat0pjet_0ptv_QCD_selection_cr_SubstrFailMassPass","QCD CR W/Z mass window 0 lep merged LP 0tag","");
  // config.addRegion("1tag1pfat0pjet_0ptv_QCD_selection_cr_SubstrFailMassPass","QCD CR W/Z mass window 0 lep merged LP 1tag","");

  // config.addRegion("0tag0pfat0pjet_0ptv_QCD_selection_cr_MassPass","QCD CR W/Z mass window 0 lep resolved 0tag","");
  // config.addRegion("1tag0pfat0pjet_0ptv_QCD_selection_cr_MassPass","QCD CR W/Z mass window 0 lep resolved 1tag","");
  // config.addRegion("2tag0pfat0pjet_0ptv_QCD_selection_cr_MassPass","QCD CR W/Z mass window 0 lep resolved 2tag","");

  // // // side-bands
  // config.addRegion("0tag1pfat0pjet_0ptv_QCD_selection_cr_SubstrPassMassFail","QCD CR W/Z mass side-bands 0 lep merged 0tag","");
  // config.addRegion("1tag1pfat0pjet_0ptv_QCD_selection_cr_SubstrPassMassFail","QCD CR W/Z mass side-bands 0 lep merged 1tag","");
  // config.addRegion("2tag1pfat0pjet_0ptv_QCD_selection_cr_MassFail","QCD CR W/Z mass side-bands 0 lep merged 2tag","");

  // config.addRegion("0tag0pfat0pjet_0ptv_QCD_selection_cr_MassFail","QCD CR W/Z mass side-bands 0 lep resolved 0tag","");
  // config.addRegion("1tag0pfat0pjet_0ptv_QCD_selection_cr_MassFail","QCD CR W/Z mass side-bands 0 lep resolved 1tag","");
  // config.addRegion("2tag0pfat0pjet_0ptv_QCD_selection_cr_MassFail","QCD CR W/Z mass side-bands 0 lep resolved 2tag","");

  // // //   // upper side-bands
  // config.addRegion("0tag1pfat0pjet_0ptv_QCD_selection_cr_SubstrPassMassFailHigh","QCD CR W/Z upper mass side-bands 0 lep merged 0tag","");
  // config.addRegion("1tag1pfat0pjet_0ptv_QCD_selection_cr_SubstrPassMassFailHigh","QCD CR W/Z upper mass side-bands 0 lep merged 1tag","");
  // config.addRegion("2tag1pfat0pjet_0ptv_QCD_selection_cr_MassFailHigh","QCD CR W/Z upper mass side-bands 0 lep merged 2tag","");

  // config.addRegion("0tag0pfat0pjet_0ptv_QCD_selection_cr_MassFailHigh","QCD CR W/Z upper mass side-bands 0 lep resolved 0tag","");
  // config.addRegion("1tag0pfat0pjet_0ptv_QCD_selection_cr_MassFailHigh","QCD CR W/Z upper mass side-bands 0 lep resolved 1tag","");
  // config.addRegion("2tag0pfat0pjet_0ptv_QCD_selection_cr_MassFailHigh","QCD CR W/Z upper mass side-bands 0 lep resolved 2tag","");

  //   // lower side-bands
  // config.addRegion("0tag1pfat0pjet_0ptv_QCD_selection_cr_SubstrPassMassFailLow","QCD CR W/Z lower mass side-bands 0 lep merged 0tag","");
  // config.addRegion("1tag1pfat0pjet_0ptv_QCD_selection_cr_SubstrPassMassFailLow","QCD CR W/Z lower mass side-bands 0 lep merged 1tag","");
  // config.addRegion("2tag1pfat0pjet_0ptv_QCD_selection_cr_MassFailLow","QCD CR W/Z lower mass side-bands 0 lep merged 2tag","");

  // config.addRegion("0tag0pfat0pjet_0ptv_QCD_selection_cr_MassFailLow","QCD CR W/Z lower mass side-bands 0 lep resolved 0tag","");
  // config.addRegion("1tag0pfat0pjet_0ptv_QCD_selection_cr_MassFailLow","QCD CR W/Z lower mass side-bands 0 lep resolved 1tag","");
  // config.addRegion("2tag0pfat0pjet_0ptv_QCD_selection_cr_MassFailLow","QCD CR W/Z lower mass side-bands 0 lep resolved 2tag","");

  // // QCD suppressed region, full selection
  // // mass peak
  // config.addRegion("0tag1pfat0pjet_0ptv_QCD_selection_sr_SubstrPassMassPass","QCD CR W/Z mass window 0 lep merged 0tag","");
  // config.addRegion("1tag1pfat0pjet_0ptv_QCD_selection_sr_SubstrPassMassPass","QCD CR W/Z mass window 0 lep merged 1tag","");
  // config.addRegion("2tag1pfat0pjet_0ptv_QCD_selection_sr_MassPass","QCD CR W/Z mass window 0 lep merged 2tag","");

  //config.addRegion("0tag1pfat0pjet_0ptv_QCD_selection_sr_SubstrFailMassPass","QCD CR W/Z mass window LP 0 lep merged 0tag","");
  //config.addRegion("1tag1pfat0pjet_0ptv_QCD_selection_sr_SubstrFailMassPass","QCD CR W/Z mass window LP 0 lep merged 1tag","");

  // config.addRegion("0tag0pfat0pjet_0ptv_QCD_selection_sr_MassPass","QCD CR W/Z mass window 0 lep resolved 0tag","");
  // config.addRegion("1tag0pfat0pjet_0ptv_QCD_selection_sr_MassPass","QCD CR W/Z mass window 0 lep resolved 1tag","");
  // config.addRegion("2tag0pfat0pjet_0ptv_QCD_selection_sr_MassPass","QCD CR W/Z mass window 0 lep resolved 2tag","");

  // // side-bands
  // config.addRegion("0tag1pfat0pjet_0ptv_QCD_selection_sr_SubstrPassMassFail","QCD CR W/Z mass side-bands 0 lep merged 0tag","");
  // config.addRegion("1tag1pfat0pjet_0ptv_QCD_selection_sr_SubstrPassMassFail","QCD CR W/Z mass side-bands 0 lep merged 1tag","");
  // config.addRegion("2tag1pfat0pjet_0ptv_QCD_selection_sr_MassFail","QCD CR W/Z mass side-bands 0 lep merged 2tag","");

  // config.addRegion("0tag0pfat0pjet_0ptv_QCD_selection_sr_MassFail","QCD CR W/Z mass side-bands 0 lep resolved 0tag","");
  // config.addRegion("1tag0pfat0pjet_0ptv_QCD_selection_sr_MassFail","QCD CR W/Z mass side-bands 0 lep resolved 1tag","");
  // config.addRegion("2tag0pfat0pjet_0ptv_QCD_selection_sr_MassFail","QCD CR W/Z mass side-bands 0 lep resolved 2tag","");

  // upper side-bands
  // config.addRegion("0tag1pfat0pjet_0ptv_QCD_selection_sr_SubstrPassMassFailHigh","QCD CR W/Z upper mass side-bands 0 lep merged 0tag","");
  // config.addRegion("1tag1pfat0pjet_0ptv_QCD_selection_sr_SubstrPassMassFailHigh","QCD CR W/Z upper mass side-bands 0 lep merged 1tag","");
  // config.addRegion("2tag1pfat0pjet_0ptv_QCD_selection_sr_MassFailHigh","QCD CR W/Z upper mass side-bands 0 lep merged 2tag","");

  // config.addRegion("0tag0pfat0pjet_0ptv_QCD_selection_sr_MassFailHigh","QCD CR W/Z upper mass side-bands 0 lep resolved 0tag","");
  // config.addRegion("1tag0pfat0pjet_0ptv_QCD_selection_sr_MassFailHigh","QCD CR W/Z upper mass side-bands 0 lep resolved 1tag","");
  // config.addRegion("2tag0pfat0pjet_0ptv_QCD_selection_sr_MassFailHigh","QCD CR W/Z upper mass side-bands 0 lep resolved 2tag","");

  // //   // lower side-bands
  // // config.addRegion("0tag1pfat0pjet_0ptv_QCD_selection_sr_SubstrPassMassFailLow","QCD CR W/Z lower mass side-bands 0 lep merged 0tag","");
  // // config.addRegion("1tag1pfat0pjet_0ptv_QCD_selection_sr_SubstrPassMassFailLow","QCD CR W/Z lower mass side-bands 0 lep merged 1tag","");
  // // config.addRegion("2tag1pfat0pjet_0ptv_QCD_selection_sr_MassFailLow","QCD CR W/Z lower mass side-bands 0 lep merged 2tag","");

  // // config.addRegion("0tag0pfat0pjet_0ptv_QCD_selection_sr_MassFailLow","QCD CR W/Z lower mass side-bands 0 lep resolved 0tag","");
  // // config.addRegion("1tag0pfat0pjet_0ptv_QCD_selection_sr_MassFailLow","QCD CR W/Z lower mass side-bands 0 lep resolved 1tag","");
  // // config.addRegion("2tag0pfat0pjet_0ptv_QCD_selection_sr_MassFailLow","QCD CR W/Z lower mass side-bands 0 lep resolved 2tag","");

  ///////////////////////////
  /// Now ready to make plots

  // check for errors in configuration
  if(!config.isValid()) {
    std::cout << "Error in configuration: ===> Aborting..." << std::endl;
    return;
  }

  // we have all what we need to know
  // ==> now make the plots
  PlotMaker plotter;
  plotter.makePlots(config);

}
