///////////////////////////////////////////////////////
//
// macro for plotting histograms made by CxAODReader
// Run using e.g.
// root -b -q -l macros/runMonoVHPlots.cxx\(0\)
//
// Prerequisites:
//  The PlottingTool
//  svn co svn+ssh://svn.cern.ch/reps/atlasoff/PhysicsAnalysis/HiggsPhys/Run2/Hbb/InputsProcessingTools/PlottingTool/trunk PlottingTool
//
/////////////////////////////////////////////////////////

#include <TSystem.h>
#include <TROOT.h>
using namespace std;

void runMonoVHPlots(int selection=0)
{

  ////////////////////////////////////////////////////////////////////////////////////////////
  // This determines which plot making function you will use
  ////////////////////////////////////////////////////////////////////////////////////////////

  cout<<"Printing plotting tool selection: "<<selection<<endl;

  ////////////////////////////////////////////////////////////////////////////////////////////
  // If you are making new plots, you will need to point the necessary path to your inputs file
  ////////////////////////////////////////////////////////////////////////////////////////////
  TString InputFileName_0Lepton       = "sys0lep_grljvt.addVars.root";
  TString InputFileName_1Lepton       = "sys1lep_grljvt.addVars.root";
  TString InputFileName_2Lepton       = "sys2lep_grljvt.addVars.root";


  ////////////////////////////////////////////////////////////////////////////////////////////
  // This is just general configuration stuff, you shouldn't need to touch this
  ////////////////////////////////////////////////////////////////////////////////////////////
  TString libdir="PlottingTool/";
  TString includePath = gSystem->GetIncludePath();
  TString InputPath = "/ptmp/mpp/pgadow/monoV/Data_Output/";
  TString input         = "BADINPUT";

  gSystem->SetIncludePath(includePath+" -I"+libdir+"transform");
  gROOT->ProcessLine(".L "+libdir+"transform/HistoTransform.C+");
  gROOT->ProcessLine(".L "+libdir+"Utils.cxx+");
  gROOT->ProcessLine(".L "+libdir+"SystTool.cxx+");
  gROOT->ProcessLine(".L "+libdir+"Config.cxx+");
  gROOT->ProcessLine(".L "+libdir+"LatexMaker.cxx+");
  gROOT->ProcessLine(".L "+libdir+"PlotMaker.cxx+");

  InputFileName_0Lepton.ReplaceAll(".root","");
  InputFileName_1Lepton.ReplaceAll(".root","");
  InputFileName_2Lepton.ReplaceAll(".root","");

  ////////////////////////////////////////////////////////////////////////////////////////////
  // This is where the plotting command is set that dictates which makePlots function will be used
  ////////////////////////////////////////////////////////////////////////////////////////////

  // selections
  if(selection==0)  input = ".x macros/makePlots0Lepton_monoVH.cxx+(\""+InputPath+"\",\""+ InputFileName_0Lepton+"\")";
  if(selection==1)  input = ".x macros/makePlots1Lepton_monoVH.cxx+(\""+InputPath+"\",\""+ InputFileName_1Lepton+"\")";
  if(selection==2)  input = ".x macros/makePlots2Lepton_monoVH.cxx+(\""+InputPath+"\",\""+ InputFileName_2Lepton+"\")";

  // ... then it is run
  gROOT->ProcessLine(input);

}
