DATE=$1

if [ -z $DATE ]; then 
	echo "source me! and provide the date to upload as YYYY-MM-DD!"
	ls /ptmp/mpp/pgadow/monoV/Plots/
else
	lsetup xrootd
	getafs
	xrdcp -r /ptmp/mpp/pgadow/monoV/Plots/${DATE} root://eosuser.cern.ch//eos/user/p/pgadow/www/share/monoV/plots/
fi
